**Quantify Day Notes**

*Date: 21/06/2023*

**Introduction**
These notes summarize discussions and considerations during Quantify Day. The purpose of Quantify is to provide a comprehensive framework for quantum algorithms, including calibration and characterization support for experimental physics. During Quantify Day we discussed and brainstormed about the current state of Quantify, potential improvements and areas of concern.

**1. Down Memory Lane -- Adriaan**
Adriaan provided some historical context surrounding Quantify. The main motivation behind developing Quantify is to address the shortcomings of existing frameworks, such as PycQED, which lacked proper documentation, maintenance, and advanced sequencing support. Adriaan emphasized the need for a mature successor to PycQED, which could seamlessly integrate pulse and gate level operations.

One of the key features discussed was the ability of Quantify to interact with hardware. Adriaan explained that the framework should enable the compilation of device and hardware configurations, with an instruction coordinator facilitating communication with the hardware. 

Recently, Quantify was used in running variational quantum algorithms (VQAs) to solve differential equations during the nanoNISQ project. Although successful, the project did uncover many bugs and limitations that needed solving. The need for addressing latency and distortion compensation during compilation was identified as a challenge. 

Originally it was decided not to use the `qcodes.Dataset` to store acquisitions as it was unstable and filled with bugs in its early development. However, we could consider adopting it while moving to more advanced applications.

Adriaan stressed that Quantify should not aim to do everything but should serve as a framework that covers basic concepts, allowing users to express their intentions clearly. Hardware corrections were acknowledged as a complex aspect that required careful consideration.

The decision to split Quantify into core and scheduler modules after approximately one year was motivated by the desire to create a more modular structure, enhancing flexibility and maintainability.

**3. Intermediate Representation -- Edgar**
The team discussed the importance of an accurate and language-independent intermediate representation for quantum compilation. Unlike classical compilation, quantum compilation faces unique challenges, such as time limits and the need to act on waveform memory. The team emphasized the significance of reusing memory and maintaining precise timing during the compilation process.

The inclusion of hardware corrections and addressing distortion emerged as critical considerations. The team highlighted the need to handle limited bits in digital-to-analog and analog-to-digital converters (DAC/ADC) and the presence of implicit sampling, which leads to discrepancies between the intended waveform and the actual output. Context-dependent corrections were identified as an important aspect to be addressed as well.

The team referenced the approach taken by Qiskit, which involves transforming an abstract syntax tree (AST) into a three-dimensional structure by incorporating timing information. The discussion also touched on the mixing of abstraction layers, real-time parameter updating, feedback mechanisms, control flow, and the importance of capturing absolute timing constraints.

Two mistakes were acknowledged: underestimating the complexity of hardware corrections and insufficient language processing, particularly concerning data acquisition and processing.

The team concluded that the intermediate representation (IR) in Quantify should be a complete and error-free structure, allowing operations to possess compiled information for better error tracing and resolution.

**4. Structure of Quantify**
The team examined the core and scheduler modules of Quantify in detail. The core module was described as a synchronous control loop responsible for data storage and maintaining live data analysis. The scheduler module, on the other hand, operated as an asynchronous control loop and provided a hardware abstraction layer, accommodating different levels of abstraction and device representation.

Concerns were raised regarding cyclic dependencies between the schedule in the scheduler and analysis in the core. The team discussed the placement of physics and engineering knowledge within the project, emphasizing the need for a cohesive structure.

The concept of a device abstraction layer within the scheduler was introduced. This layer would accommodate quantum device abstraction, allowing for experiments on specific device elements, such as NV centers, and enabling the ability to sweep voltages when the schedule execution is not required.

The team identified areas where improvements were needed, such as the complexity of analysis, lack of customizable live visualization, and the need for soft averaging in experiments. The suggestion was made to combine graphical user interface (GUI) adaptations with automated scripting.

Concerns were raised about the core and scheduler modules containing hard-coded superconducting qubit code, which could be confusing for users working on different platforms or with different types of qubits, such as NV centers. Similar concerns were expressed about the presence of specific analysis classes within the core module.

The need for device abstraction within the core module was emphasized, allowing for mapping between device elements and physical instruments. Formalizing abstraction layers and providing information on connecting flux bias lines to device elements and hardware configurations were considered essential.

The team deliberated on whether the scheduler should primarily handle schedules with timings, abstraction layers, or experiment descriptions. The possibility of performing vector network analyzer (VNA) traces without involving the scheduler was also discussed, suggesting that the VNA could receive and return trace information directly.

The question of whether the core module should be useful even without device configuration and whether trivial experiments defined in the core should still be compiled in the scheduler was left open for further consideration.


*Note: The report is based on the provided notes and may not include all details or capture the full context of the discussions. Further refinement and clarification may be required based on additional information and discussions.*

## Present:

### Maintainers and Project Owners

- [ ] Kelvin
- [ ] Luis
- [ ] Damien
- [ ] Victor
- [ ] Slava
- [ ] Adriaan (PO)
- [ ] Jules (PO)

### Community

- [ ] Jordy
- [ ] Adam
- [ ] Diogo
- [ ] Gijs
- [ ] Damaz

## Introduction new member:

## Highlights:
- precommit hooks are now back to just single stage (black)\
     Commited by interns Gijs and Michiel


### Quantify-scheduler:
- Present milestones. (https://gitlab.com/quantify-os/quantify-scheduler/-/milestones/3)
- ZI backend working with known constraints. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/263)
    - Work on cleaning up the MR has completed.\
        Remove all to-do warnings. Documentation is still missing. It can be done with a seperate MR.

Next week the LO will be tackled by Kelvin



#### Merged MRs (highlights)


### Quantify-core:
- Approximately 1 month left (https://gitlab.com/quantify-os/quantify-core/-/milestones/9)\
     Still roadblocked by bandwith issues. \
     Dataset V2 is still being worked on. Work is on making the xarray subclass.

#### Merged MRs (highlights)

## Requested discussions:


## AOB:
- (Requested Kelvin) Kelvin won't be in NL from Dec 20 - Feb 20. Switch timing of MM?\
     The meeting will be switched to Friday 11 am CET.

## Pending topics (for next MMs):

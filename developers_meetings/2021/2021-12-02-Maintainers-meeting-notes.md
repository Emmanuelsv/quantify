## Present:

### Maintainers and Project Owners

- [ ] Kelvin
- [ ] Luis
- [ ] Damien
- [ ] Slava
- [ ] Adriaan (PO)
- [ ] Jules (PO)

### Community

- [ ] Jordy
- [ ] Adam
- [ ] Diogo
- [ ] Gijs
- [ ] Damaz

## Introduction new member:
- Vraj Patel from OrangeQS

## Highlights:
- ZI backend has been merged. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/263)

### Quantify-scheduler:
- Present milestones. (https://gitlab.com/quantify-os/quantify-scheduler/-/milestones/3)
- LO issue still tackled by Kelvin. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/283)
- Time for another official release. (v0.5.2)
- (Requested by Kelvin) How about the MR for ScheduleGettableSinglechannel? (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/260)
  - Choose Damien's second option (extra function in the interface)
  - Assign to Damaz (finish by end of December)


#### Merged MRs (highlights)


### Quantify-core:
- Approximately 1 month left (https://gitlab.com/quantify-os/quantify-core/-/milestones/9)\
     Still roadblocked by bandwith issues. \
     Dataset V2 is still being worked on. Work is on making the xarray subclass.
     - Roadblocking this communication as well. (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/274)
     - Merge Pieter's MR and replace when dataset V2 is ready 
- New decoupled remote plotmon crashes occasionally in Windows systems. (https://gitlab.com/quantify-os/quantify-core/-/issues/285)
  - #251  also related


#### Merged MRs (highlights)

## Requested discussions:
- (Requested by Kelvin) How to manage the quantify experiment external MR. (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/274)

## AOB:

## Pending topics (for next MMs):

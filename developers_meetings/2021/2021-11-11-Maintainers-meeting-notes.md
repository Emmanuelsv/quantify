## Present:

### Maintainers and Project Owners

- [x] Kelvin
- [x] Luis
- [x] Damien
- [ ] Victor
- [x] Slava
- [x] Adriaan (PO)
- [x] Jules (PO)

### Community

- [x] Jordy
- [x] Adam
- [x] Diogo
- [x] Gijs
- [x] Damaz

## Introduction new member:

## Highlights:
- Quantify-scheduler v0.5.1 has been released. (https://gitlab.com/quantify-os/quantify-scheduler/-/releases/0.5.1)

### Quantify-scheduler:
- Present milestones. (https://gitlab.com/quantify-os/quantify-scheduler/-/milestones/3)
    - GenericInstrumentCoordinatorComponent implemented.
- More work towards the ZI backend. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/263)
    - Lots of progress. HDAWG done and UHFQA should be done soon. (E.g. timing table)
    - Docs failing. Any hints?
      - Potentially a problem with autodoc-typehints

#### Merged MRs (highlights)
- Qblox backend docs has been updated and improved. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/247)
- Qblox backend fix to the pulse stitching with modulation. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/264)


### Quantify-core:
- Dataset v2 implementation MR is quite behind. Need review soon. (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/243)
  - Slava to pick up review again.

- (Requested by Adriaan) We need maintainers to push key features in case we forget them whilst in the middle of hotfixing bugs. Example would be the dataset V2 which Slava can take up the responsibility to push and assign us to work on issues related to the milestone instead of potentially missing out these features while we're busy hotfixing bugs.


#### Merged MRs (highlights)
-

## Requested discussions:
- (Requested by Kelvin) PubSub for MC in quantify-core. (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/265)

## AOB:
- (Requested by Damien) Addressing installation notes again as it may be outdated. Triggered by a user question to qbox support. No single solution yet in mind as it could be various causes to the issue faced by the user. Suggest to forward the slack channel over to the user.

## Pending topics (for next MMs):
-

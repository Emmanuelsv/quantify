## Present:

### Maintainers and Project Owners

- [X] Kelvin
- [X] Luis
- [X] Damien
- [X] Victor
- [X] Slava
- [ ] Adriaan (PO)
- [X] Jules (PO)

### Community

- [X] Jordy
- [X] Adam
- [X] Diogo
- [ ] Gijs
- [X] Damaz

## Introduction new member:

## Highlights:
-

### Quantify-scheduler:

-

#### Merged MRs (highlights)

- Added staircase test programs for both backends to be run as a test in the hardware setup hence, exposed to the user.
    - https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/222

- ZI Backend
    - Several more fixes with Adriaan being in Germany to also fix and test the implementation on actual setup.

- Qblox Backend
    - Updated to support qblox-instruments v0.5.2


### Quantify-core:

#### v0.6 milestone

- Milestone board: https://gitlab.com/quantify-os/quantify-core/-/milestones/9
- Target: End of October

- Dataset spec finished this week, Ready for review.
    - https://gitlab.com/quantify-os/quantify-core/-/merge_requests/224
    - https://quantify-quantify-core--224.com.readthedocs.build/en/224/technical_notes/dataset_design/index.html
- Xarray subclass prototyping started (Luis).
    - https://gitlab.com/quantify-os/quantify-core/-/issues/254

- Kelvin needs to review the spec by Monday.

#### Merged MRs (highlights)

- No longer using `OrderedDict`. Use `dict` instead.
    - https://gitlab.com/quantify-os/quantify-core/-/merge_requests/237


## Requested discussions:
- (Requested by Victor)
    - New quantify-scheduler release in pypi?
    - Decision: Make a release in pypi by next week. Reason: lots of changes made to qblox and zi backends.


## AOB:
-

## Pending topics (for next MMs):
- Agree on procedure for hacks. Branching practices for example.

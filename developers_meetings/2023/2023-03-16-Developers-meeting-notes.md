## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize hardware configuration** (spearheaded by: Tobias)
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Robert, Edgar, Daniel)
  - We aim to deliver a "connecting sched & core" "what is possible with Quantify" showcase depicting T1 experiment on Qblox hardware :hourglass:
    - Next to that, we are adding Qblox Quantify showcases for transmon, spin, NV centers to [Qblox RTD](https://qblox-qblox-instruments.readthedocs-hosted.com/en/master/) :hourglass:
  - _We are going over the existing tutorials of quantify-scheduler and bringing them up-to-date/removing depr code_ :white_check_mark:
- **Parallel work**:
  1. _[milestone 3](https://gitlab.com/groups/quantify-os/-/milestones/3)  Quantify Platform release aka The Website :tm: (remaining: website + RTD polish)_ :white_check_mark:
  1. Clean up open tickets (~~247~~ **235** open [board](https://gitlab.com/groups/quantify-os/-/boards)) :hourglass:


## Highlights

New releases `quantify-core==0.7.1` and `quantify-scheduler==0.12.3`
- Bump qcodes requirement to >=0.37
- Patch releases to fix some documentation issues, in support of the, new official Quantify website: quantify-os.org

Enabled ruff and pyright linters (quantify-core!441, quantify-scheduler!614)

For 10k operations, creating the schedule is now 4 times faster:
- make name uniqueness check of schedulables more readable and efficient (quantify-scheduler!631)

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Improve efficiency of plot_pulse_diagram for matplotlib backend (QAE-967) (quantify-scheduler!629)
- Resolve "All `CompilationNodes` should accept the full `CompilationConfig` as input" (quantify-scheduler!615)

## Quantify-core

### Merged MRs (highlights)

- Display instrument labels in insmon (quantify-core!369) :rocket: :rocket: :rocket:

## Requested discussions / Any other business

### Marker behavior in Qblox backend (@dvos2, @ThomasMiddelburg)

>  Introduces a potentially breaking change to the behaviour of markers https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/606
>  - This MR introduces the `enable_marker` param to the hw config

Potentially breaking change is acceptable, the functionality is rarely used currenty.

> IQ convention change

- Maintainers must be more careful with releasing bugfix releases: bugfix must be *really* a bugfix releases, and not bumping requirements "oh by the way".
- We need a hardware test for IQ convention, perhaps with analyzing frequency sweep.

> MR quantify-scheduler!581

~~Check with Francesco what is going on.~~

Just low on a priority list of the author.

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

*(empty)*


# Epics (APSMM23 release)

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize hardware configuration** (spearheaded by: Hiresh/Tobias)
  - Tobias will come with an updated plan on what will be done first and what is aimed to finish before APSMM
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Daniel)
  - We aim to deliver a "connecting sched & core" "what is possible with Quantify" showcase depicting T1 experiment on Qblox hardware
    - Next to that, we are adding Qblox Quantify showcases for transmon, spin, NV centers to [Qblox RTD](https://qblox-qblox-instruments.readthedocs-hosted.com/en/master/)
  - We are going over the existing tutorials of quantify-scheduler and bringing them up-to-date/removing depr code
- **Parallel work**:
  1. [milestone 3](https://gitlab.com/groups/quantify-os/-/milestones/3)  Quantify Platform release aka The Website :tm: (remaining: website + RTD polish)
  1. Clean up open tickets (~~260~~ **249** open and counting [board](https://gitlab.com/groups/quantify-os/-/boards))


## Highlights

*(empty)*

### Breaking changes

- Schedule visualization - Changed recommended way to plot circuit and pulse diagrams, old functions are deprecated. (quantify-scheduler!599)

## Quantify-scheduler

### Merged MRs (highlights)

- Schedule visualization - Deprecate `visualization` module and its functions `circuit_diagram_matplotlib`, `pulse_diagram_matplotlib`, and `pulse_diagram_plotly`. Create `_visualization` submodule within `schedules`. Make plots via `ScheduleBase` methods. Move visualization tests to `schedules` directory, and make tests for `ScheduleBase` plotting methods. (quantify-scheduler!599)
- JSON utilities - Introduce serialization of python objects to a dotted import string, next to the already existing deserialization (`import_python_object_from_string`). Serialization now happens automatically in `DataStructure`s, deserialization is implemented using Pydantic validators in the configuration models. (quatnify-scheduler!578)
- Qblox backend - Set the `marker_ovr_en` QCoDeS parameter by default to `False` before schedule execution, so that the markers are always controlled using the `MarkerConfiguration` (quantify-scheduler!576)
- ZI LabOne backend - Fix documentation of trigger configuration (quantify-scheduler!607)
- ZI LabOne backend - Support distortion corrections (quantify-scheduler!600)
- Documentation - Qblox backend reference guide overhaul and fix docs generation warnings (quantify-scheduler!587)

## Quantify-core

### Merged MRs (highlights)

*(empty)*

## Requested discussions / Any other business

*(empty)*

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

*(empty)*


## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)

## Highlights

- Both `quantify-scheduler` and `quantify-core` now use the PyData sphinx-theme! 🎂🎉👯🎊

### Breaking changes

## Quantify-scheduler

### Merged MRs (highlights)

- Documentation
 - Switch to PyData sphinx theme (quantify-scheduler!778)
 - Adjust titles and some other plumbing to align with core (quantify-scheduler!786)
 - Introduce reference guide and extend contrib guidelines (quantify-scheduler!790)
- Add Chevron CZ schedule function (quantify-scheduler!700)

## Quantify-core

### Merged MRs (highlights)

- Documentation
 - Extend contribution guidelines (quantify-core!489)
 - Adjust titles and some other plumbing to align with sched (quantify-core!488)
- Partial Linting of SI_utilities module using Ruff (quantify-core!483)

## Review open (external) MRs

- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Requested discussions / Any other business

### BasicTransmonElements are cleaned up when initialized with only weak reference

> @tobiasbonsen https://gitlab.com/quantify-os/quantify-scheduler/-/issues/442

Tobias: 
- Naive solution: in the quantum device add reference to the device elements

Edgar: 
- Problem is present for any qcodes instrument, as all_instruments holds weak references
- Naive solution will impair closing of instruments via qcodes close()

Thomas: 
- close is for instruments. Do we care about that for device elements?

Outcome:
- Add private reference list in quantum device 
- Add comment why it's there


## Pending topics (for next DMs)

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset

### Overview of current state of Acquisition redesign and work remaining

> @ereehuis promises to provide this upon new RawDataset implemented in quantify-scheduler
> Upcoming first milestone: set of agreed upon requirements for RawDataset and ProcessedDataset by August 1st

### Reorganization of schedule library

> @ereehuis, @djweigand

### Presentation of Qiskit integration

> https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/764
> @axean


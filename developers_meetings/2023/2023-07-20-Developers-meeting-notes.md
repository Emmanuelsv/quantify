## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Robert, Edgar, Daniel)

## Highlights

`quantify-scheduler-0.15.0` has been released! See the [changelog](https://gitlab.com/quantify-os/quantify-scheduler/-/blob/main/CHANGELOG.md#0150-2023-07-13) for details.

We have disabled Python 3.10 testing for Quantify pipelines, because 3.10 is
currently unsupported in `quantify-scheduler`.

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Pin version of `dataclasses-json` due to breaking pipelines. (quantify-scheduler!727)
- Schedulables - store references to `Schedulables` in timing contraints as `string` rather than `Schedulable`.  (quantify-scheduler!717)

## Quantify-core

### Merged MRs (highlights)

*(empty)*

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Requested discussions / Any other business

### Expected behaviour for overwriting pulses

> Context: https://gitlab.com/quantify-os/quantify-scheduler/-/issues/436
>
> What is the expected behaviour for the case when user defines seveal pulses
> that overlap? Do we need to raise a warning, an error, sum them up, silently overwrite?

## Pending topics (for next DMs)

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset

### Overview of current state of Acquisition redesign and work remaining

> @ereehuis promises to provide this upon new RawDataset implemented in quantify-scheduler
> Upcoming first milestone: set of agreed upon requirements for RawDataset and ProcessedDataset by August 1st


## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)

## Highlights

*(empty)*

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Documentation - Elaborated on loop limitations. (quantify-scheduler!823)
- Compilation - Add `keep_original_schedule` compilation option into the `CompilationConfig`, controlling whether a copy of the schedule is made at the start of compilation (quantify-scheduler!816)
- Compilation - Added an optional `reference_magnitude` parameter to `VoltageOffset` operations (quantify-scheduler!797)
- Documentation - Add a short explanation and example of the `NumericalWeightedIntegrationComplex` protocol to the Acquisitions tutorial. (quantify-scheduler!791)

## Quantify-core

### Merged MRs (highlights)

*(empty)*

## Review open (external) MRs

- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Requested discussions / Any other business

*(empty)*

## Pending topics (for next DMs)

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset

### Overview of current state of Acquisition redesign and work remaining

> @ereehuis promises to provide this upon new RawDataset implemented in quantify-scheduler
> Upcoming first milestone: set of agreed upon requirements for RawDataset and ProcessedDataset by August 1st

### Reorganization of schedule library

> @ereehuis, @djweigand


## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)

## Highlights

*(empty)*

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Compilation - Made the `HardwareCompilationConfig` datastructure backend-specific (quantify-scheduler!708)
- Compilation - Change default `ref_pt` and `ref_pt_new` to `None` in `Schedule` and `Schedulable`; `compilation.determine_absolute_timing` will then assume end and start respectively, conserving previous behavior (quantify-scheduler!733)
- Qblox backend - Prevent uploading "null" (i.e. all-zero) waveforms (quantify-scheduler!711)
- Qblox backend - Fix playback on both outputs of a QCM-RF (quantify-scheduler!742)

## Quantify-core

### Merged MRs (highlights)

*(empty)*

## Review open (external) MRs

- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Requested discussions / Any other business

### Semi-automated frequent releases

> @AdriaanRol Discussion goal: probe what people think of it

Issue:
- Releases are hard and infrequent
- We like users to use released versions, instead work from main

Proposed solution: Frequent releases
- Weekly release schedule 
- main branch should always be in a releaseable state

Currently for OQS, process is:
- Weekly release: update changelog, hit button
- Also: Additional hotfix/patch release when required

Main implicit benefit: Focus should be ambition to release weekly
- Releasing frequently makes that the processes allowing one to do so will have to follow along
- Leads to implicit quality improvements and better, simpler release processes

Edgar:
- No brainer after we've merged core and sched and cut off the backends
- But merge and cut need to be done first

## Pending topics (for next DMs)

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset

### Overview of current state of Acquisition redesign and work remaining

> @ereehuis promises to provide this upon new RawDataset implemented in quantify-scheduler
> Upcoming first milestone: set of agreed upon requirements for RawDataset and ProcessedDataset by August 1st


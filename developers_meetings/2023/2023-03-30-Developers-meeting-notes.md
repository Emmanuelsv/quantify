## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize hardware configuration** (spearheaded by: Tobias)
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Robert, Edgar, Daniel)
  - New Tutorial: Running an Experiment
    - "connecting sched & core" "what is possible with Quantify" showcase depicting T1 experiment on Qblox hardware :hourglass:
    - Preview: https://quantify-quantify-scheduler--645.com.readthedocs.build/en/645/tutorials/Running%20an%20Experiment.html

- **Parallel work**:
  1. Clean up open tickets (~~236~~ **235** open [board](https://gitlab.com/groups/quantify-os/-/boards)) :hourglass:

## Highlights

### Breaking changes

We are refactoring config format, that may lead to the need of adjustments if we are not careful enough with backwards compatibility (we are trying to not break anything straight away and throw future warnings instead).

## Quantify-scheduler

### Merged MRs (highlights)

- Compilation - Move `LatencyCorrections` to `CompilationConfig.hardware_options` and use field name `corrections` instead of `latencies` (!633).
- Compilation - Move `DistortionCorrections` to `CompilationConfig.hardware_options` (!648)
- ... and some minor maintenance MRs

## Quantify-core

### Merged MRs (highlights)

Couple of minor maintenance MRs

## Requested discussions / Any other business

### Quantify Day 2023

Proposal do to new Quantify (half) day in June

Would start with presentation on: What is Quantify by Mr Rol himself

Aims for the day:
- Simply, teambuilding, all developers are invited (so we'll do in person, but allowing for joining remotely)
- brainstorm sessions on possible features and problems in Quantify
- discussing the epics
- identifying issues and setting priorities
- transparency on what Qblox / OQS is developing 


### Changing Read-the-Docs theme to pydata

All in favor in changing theme to pydata, need to identify the issues that surface due to that and weigh against the issues it solves

Robert has prepped preview for scheduler: https://quantify-quantify-scheduler--656.com.readthedocs.build/en/656/


## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

*(empty)*


## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize hardware configuration** (spearheaded by: Tobias)
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Robert, Edgar, Daniel)

- **Parallel work**:
  1. Clean up open tickets (~~235~~ **232** open [board](https://gitlab.com/groups/quantify-os/-/boards)) :hourglass:


## Highlights

Maintainers discussed that we want to have [documentation epic](https://gitlab.com/groups/quantify-os/-/epics/2) to be closed soon. This requires writing/finishing a couple of tutorials.

- Qblox backend - `Measure` can now use the `NumericalWeightedIntegrationComplex` protocol. The `DispersiveMeasurement` has been expanded with optional weight parameters for use in this protocol (quantify-scheduler!612).
- Operations - Introduce the `StitchedPulse`, an Operation that can be composed of AWG offset instructions and waveforms, and the `StitchedPulseBuilder` which can be used to create `StitchedPulse`s (quantify-scheduler!588). Additionally, helper functions `long_square_pulse`, `long_ramp_pulse` and `staircase_pulse` are introduced in `quantify_scheduler.pulse_factories`, to more easily generate the operations for these common use-cases.
- Operations, Qblox backend - Introduce the `VoltageOffset` operation, for use in the `StitchedPulse`, and modify the compilation steps to compile this operation (quantify-scheduler!588).

### Breaking changes

- Qblox backend - Deprecate the `instruction_generated_pulses_enabled` hardware configuration setting, as well as the `StitchedSquarePulseStrategy` and `StaircasePulseStrategy`. The newly introduced `StitchedPulse`, as well as the helper functions in `quantify_scheduler.pulse_factories` can be used instead. (quantify-scheduler!637).
- Qblox backend - Default marker behaviour has been changed, though, nobody we know seems to be affected. (quantify-scheduler!662). This change was approved on a developers meeting some time ago.

## Quantify-scheduler

### Merged MRs (highlights)

- Compilation - Update structure of `HardwareOptions` datastructure with fields `latency_corrections: Dict[str, LatencyCorrection]` and `distortion_corrections: Dict[str, DistortionCorrection]` (quantify-scheduler!650).
- Tests - Move Qblox Pulsar entries from `qblox_test_mapping.json` to pytest fixtures (quantify-scheduler!632)
- New Tutorial: Running an Experiment (quantify-scheduler!645)
- Qblox backend - `SquarePulse`s with a duration >1 microsecond (a constant in the Qblox backend) are compiled to AWG offset instructions (quantify-scheduler!637).
- Qblox backend - `Measure` can now use the `NumericalWeightedIntegrationComplex` protocol. The `DispersiveMeasurement` has been expanded with optional weight parameters for use in this protocol (quantify-scheduler!612).
- Operations - Introduce the `StitchedPulse`, an Operation that can be composed of AWG offset instructions and waveforms, and the `StitchedPulseBuilder` which can be used to create `StitchedPulse`s (quantify-scheduler!588). Additionally, helper functions `long_square_pulse`, `long_ramp_pulse` and `staircase_pulse` are introduced in `quantify_scheduler.pulse_factories`, to more easily generate the operations for these common use-cases.
- Operations, Qblox backend - Introduce the `VoltageOffset` operation, for use in the `StitchedPulse`, and modify the compilation steps to compile this operation (quantify-scheduler!588).

## Quantify-core

### Merged MRs (highlights)

*(empty)*

## Requested discussions / Any other business

### @djweigand: Analysis classes

Goal: write tutorial for the qubit tuneup up to two-qubit gate.
Problem: analysis classes are undocumented.

- @slavoutich: we will need to rewrite analysis classes anyway
- @djweigand: we need to provide documentation as a stop-gap solution

Axel can document current analysis classes.
@slavoutich oranizes technical meeting on analysis refactoring.

### @djweigand: quantify-scheduler!652 takeover

@adamorangeqs approved, but @slavoutich requested changes and @djweigand goes for vacation.

@slavoutich and @tobiasbonsen need to discuss this with @adamorangeqs and take over, if needed.
If we decide to take over and finish it, we'll loop through @rsokolewicz.

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

*(empty)*


## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Robert, Edgar, Daniel)

## Highlights

*(empty)*

### Breaking changes

- Acquisition - Trigger count protocol changes: `ScheduleGettable` and `InstrumentCoordinator` return data reconciliation; using `counts` instead of `acq_index` as a dimension when `BinMode.AVERAGE` is used (quantify-scheduler!703)

## Quantify-scheduler

### Merged MRs (highlights)

- Compilation - Restructured `CompilationConfig` by adding `HardwareCompilationConfig` datastructure that contains `HardwareDescription`, `Connectivity`, and `HardwareOptions` (quantify-scheduler!680)
- Documentation - Utilize `hvplot` and `bokeh` for part of data visualization in documentation to overcome issues with `matplotlib` (quantify-scheduler!712).
- Documentation - Add documentation for a reference magnitude parameter (quantify-scheduler!712).
- Documentation - A new `ScheduleGettable` tutorial has been added (quantify-scheduler!686).
- Visualization - Large refactor of the pulse diagram, mostly in the matplotlib backend (quantify-scheduler!664).
  - The matplotlib backend no longer plots 0 V points in between pulses, leading to significant performance improvements in some cases.
  - In both the matplotlib and the plotly backend, lines are now shaded from the line to the x-axis.
  - For the matplotlib backend, an extra keyword argument 'multiple_subplots' (bool) is added. If True, each port used in the schedule gets its own subplot, similar to how it's done in the plotly backend.
  - In the plotly backend, the time slider at the bottom of the figure has been removed. This was necessary to allow the y-axis of the bottom-most plot to be interactively re-scalable as well.

## Quantify-core

### Merged MRs (highlights)

- Replace usage of the deprecated `qcodes.plots` with `qcodes_loop.plots` in the remote plot monitor (quantify-core!465)
- Visualization - Minor refactor to make visualization module compatible with `scipy>=1.11` (quantify-core!466)

## Requested discussions / Any other business

### Pydantic-2.0.0 released

The release is quite breaking and we want to give devs some time to fix bugs.

**Decision**: wait a couple of month with migrating to `pydantic>=2.0.0`, pin `pydantic<2` for now.

### Qblox firmware update is coming

There is a breaking release going, meaning that people will need to bump `qblox-instruments` requirement in `quantify`.
This release improves the freedom and convenience for input/output configuration.

**Decision**: we are going to do two sequential releases, without new `qblox-instruments` version and with it.

### Something is wrong with instrument monitor

When user does ramping on QCM, instrument monitor freezes. It can be killed, but that is a major inconvenience for a user.

Likely there is some race condition in networking. We need to investigate deeper possible fixes.

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset


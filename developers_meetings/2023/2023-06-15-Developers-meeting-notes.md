## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Robert, Edgar, Daniel)

- **Parallel work**:
  1. Clean up open tickets (~~205~~ **205** open [board](https://gitlab.com/groups/quantify-os/-/boards)) :hourglass:
  2. Make working with docuemntation convenient.

## Highlights

*(empty)*

### Breaking changes

- Qblox backend - Remove overwriting of IF frequency to `None` when `mix_lo=False` (quantify-scheduler!699)

## Quantify-scheduler

### Merged MRs (highlights)

- Compilation - Changed units of amplitude parameters in device elements to dimesionless, for consistency with new `ReferenceMagnitude` interface (quantify-scheduler!691).
- Deprecation - Deprecated code that had been scheduled to be removed after version 0.13 has been removed. The deprecated code suggestions have been updated (quantify-scheduler!679).
- Compilation - Amended `ReferenceMagnitude` set method to ensure that all unit parameters are not overwritten when one of the parameters is set to `nan` (quantify-scheduler!695, quantify-scheduler#429).
- Waveforms - Fix `sudden_net_zero` waveform generation. Rounding of pulse times will now no longer lead to an incorrect SNZ pulse. I.e., the last sample of the first pulse and the first sample of the second pulse will remain correctly scaled, and the integral correction will have an amplitude such that the integral of the pulse is zero. (quantify-scheduler!581, quantify-scheduler#310)
- Qblox backend - Remove overwriting of IF frequency to `None` when `mix_lo=False` (quantify-scheduler!699)
- JSON utilities - `DataStructure` can serialize Numpy arrays using new `quantify_scheduler.structure.NDArray` field type. (quantify-scheduler!701)

## Quantify-core

### Merged MRs (highlights)

*(empty)*

## Requested discussions / Any other business

### @djweigand: About `InstrumentMonotor.update_snapshot()

> Short discussion point for tomorrow's meeting: is anybody using InstrumentMonitor.update_snapshot=True? I.e. changing the default value. This is unsafe, and can crash Instruments due to collisions. I can't join tomorrow, but would like to ask if we can remove this parameter and force using False, as there is no safe way to use the parameter. Currently crashes are rare, but they will become very likely with upcoming features.
> See also [the qcodes documentation](https://qcodes.github.io/Qcodes/examples/DataSet/Threaded%20data%20acquisition.html).

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset


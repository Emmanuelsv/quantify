## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)

## Highlights

Both [quantify-core](https://quantify-os.org/docs/quantify-core/latest/) and [quantify-scheduler](https://quantify-os.org/docs/quantify-scheduler/latest/) documentation have the new locations.
These will be official soon, with ReadTheDocs being eventually disabled.

### Breaking changes

- Acquisition - `WeightedIntegratedComplex` and `NumericalWeightedIntegratedComplex` now return normalized integrations, similar to `SSBIntegrationComplex` (quantify-scheduler!801)

## Quantify-scheduler

### Merged MRs (highlights)

- Infrastructure - Bump `quantify_core` version dependency to 0.7.1 to include the fix to `without()` in quantify-corequantify-scheduler!438. (quantify-scheduler!795)
- Plotting - Fix error while plotting numerical pulses with non-zero rel_time (quantify-scheduler!783)
- Acquisition - `WeightedIntegratedComplex` and `NumericalWeightedIntegratedComplex` now return normalized integrations, similar to `SSBIntegrationComplex` (quantify-scheduler!801)

## Quantify-core

### Merged MRs (highlights)

- Infrastructure - Remove dependency on `pendulum` (quantify-core!491)
- Data - Fix a bug in instrument names generation indoduced in quantify-core!478. (quantify-core!486)

## Review open (external) MRs

- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Requested discussions / Any other business

### Use of qubit param in pre-defined schedules where qubit name is meant

> Edgar, Nicholas

Can we change to `qubit_name` throughout?

`element_name` might be a better general name as we aren't always measuring qubits
- People typically have a set of kwargs that they feed into multiple schedules, hence naming needs to be uniform

trace acq schedule, what var name is used there?
- could be that is already only using port clock
- Actually, a mix is used, see https://gitlab.com/quantify-os/quantify-scheduler/blob/cf4246f5e7fa185fcf159bf39674ceaa6d897b38/quantify_scheduler/schedules/trace_schedules.py

Talk to Qblox QAE dept to see how they think about the value of changing this. Slava sees limited value (and these schedules should be moved out of main quantify package to some experiments library in his view)

### Measurement control: parallel measurements, multiple feed lines at the same time

> Sean

2D scans, possible for 2 settable parameters, not possible if you have 3 feed lines, you need two 2D scans

Can use this helper function as inspiration: 

https://quantify-os.org/docs/quantify-core/latest/api/measurement.html#quantify_core.measurement.control.grid_setpoints

takes two parameters and zips them

setpoints for one fleet line and other feedline


### Timing table order

> Hiresh

order in the timing table is sorted to when it is added to the schedule, not on abs timing

order by default on abs timing?

should be easy way to switch, so method would need to be added



## Pending topics (for next DMs)

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset

### Overview of current state of Acquisition redesign and work remaining

> @ereehuis promises to provide this upon new RawDataset implemented in quantify-scheduler
> Upcoming first milestone: set of agreed upon requirements for RawDataset and ProcessedDataset by August 1st

### Reorganization of schedule library

> @ereehuis, @djweigand


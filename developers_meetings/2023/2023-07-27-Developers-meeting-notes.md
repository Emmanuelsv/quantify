## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)

## Highlights

*(empty)*

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Acquisition - New acquisition protocol for thresholded acquisitions: `ThresholdedAcquisition` (quantify-scheduler!617)
- Qblox backend - Fix weighted acquisition in append mode. (quantify-scheduler!725)
- Instrument Coordinator - Improve error message for missing IC components (quantify-scheduler!718)

## Quantify-core

### Merged MRs (highlights)

*(empty)*

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Requested discussions / Any other business

### Timo: ALAP/ASAP scheduling

> @Timo_van_Abswoude: I'd like to discuss some ideas we are having on modifying determine_absolute_timing (quantify-scheduler!729)

ALAP: add to device config

default ASAP

enum timing_strategy ALAP and ASAP


first MR: investigate:

set ref_op default to None

and then in `determine_absolute_timing` set the `ref_ops`, based on ALAP or ASAP

## Pending topics (for next DMs)

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset

### Overview of current state of Acquisition redesign and work remaining

> @ereehuis promises to provide this upon new RawDataset implemented in quantify-scheduler
> Upcoming first milestone: set of agreed upon requirements for RawDataset and ProcessedDataset by August 1st


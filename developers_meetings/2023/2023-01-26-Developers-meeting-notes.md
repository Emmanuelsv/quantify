## Epics (APSMM23 release)

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize hardware configuration** (spearheaded by: Edgar)
  - https://gitlab.com/quantify-os/quantify-scheduler/-/issues/332 Update CompilationConfig with high-level structure
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Daniel)
- **Parallel work**:
  1. [milestone 3](https://gitlab.com/groups/quantify-os/-/milestones/3)  Quantify Platform release aka The Website :tm: (remaining: website + RTD replacement)
  1. Remove deprecated code / usage
  1. Clean up open tickets (~~247~~ **256** open and counting [board](https://gitlab.com/groups/quantify-os/-/boards))
  1. Fast feedback in Quantify (first MRs) (Qblox)
  1. https://gitlab.com/quantify-os/quantify-scheduler/-/issues/320 ProfiledScheduleGettable needs refactoring (Qblox)
  1. https://gitlab.com/quantify-os/quantify-scheduler/-/compare/main...QAE-472-Draft-parallel-hardware-prepare-support (Qblox)
  1. ...

## Highlights

The most work right now is going into documentation improvements.

### Breaking changes

- `quantify-core` requires `qcodes>=0.35` now. (quantify-core!427)

## Quantify-scheduler

### Merged MRs (highlights)

- Qblox backend - Add preparation of acquisition settings and accompanying datastructures for NV centers (quantify-scheduler!567)
- Qblox Backend - Added method for gain configuration, overriding gain now raises ValueError (quantify-scheduler!533)
- Schedule Functions - make experiment-related schedule functions available in `quantify_scheduler.schedules` (quantify-scheduler!572)
- Qblox backend - Add preparation of acquisition settings and accompanying datastructures for NV centers (quantify-scheduler!567)
- Compiler - Improved handling of clocks in the compiler (quantify-scheduler!538)

## Quantify-core

### Merged MRs (highlights)

- Visualization - Added exception handling for `SI_utilities.value_precision`. Should improve plotmon stability. (#350, !442)
- Requirements - `quantify-core` requires `qcodes>=0.35` now. (quantify-core!427)

## Requested discussions / Any other business

### `isort` strikes again + possible migration from Codacy

@ThomasMiddelburg:

> ...
> I saw this flying by in the software-for-bots channel (@Edgar Reehuis). Should we automate this by adding isort as a pre-commit hook?

@rsokolewicz:

> we should run isort on all files before we implement the git hook. Else every small change that we make will fail the pipeline ^^.
> at some point I would like to have ruff enabled as a pre-commit, which executes isort, pylint and a bunch of other linters/code checkers.
> Shameless self-promotion: https://gitlab.com/quantify-os/quantify-scheduler/-/issues/395 :p

@slavoutich:
Balancing act: code quality vs ease of first use
Therefore, in the past decision was made to only run black as pre-commit

@ereehuis
Okay, makes sense, let's do explore the Ruff option as replacement for our linters


## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

*(empty)*


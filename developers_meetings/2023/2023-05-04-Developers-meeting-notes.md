## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
  
  Edgar will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
  - requirements for the raw dataset
  - requirements for the processed dataset
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Robert, Edgar, Daniel)

- **Parallel work**:
  1. Clean up open tickets (~~232~~ **222** open [board](https://gitlab.com/groups/quantify-os/-/boards)) :hourglass:
  
     A new label `Stale` will be introduced and all old tickets that haven't seen updates for a long time be assigned that => they will then no longer be tracked by the maintainers but not closed as they may still have value and remain searchable (if you feel it was put on by mistake, plz post a message in the ticket)

## Highlights

`quantify-core-0.7.2` is released, fixing installation with `qcodes-0.38`.

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Operations - Introduce the `StitchedPulse`, an Operation that can be composed of AWG offset instructions and waveforms, and the `StitchedPulseBuilder` which can be used to create `StitchedPulse`s (quantify-scheduler!588, quantify-scheduler!666).
- Qblox backend - Add the `marker_debug_mode_enable` parameter to the hardware configuration, which toggles markers at the start of operations on inputs and outputs where it is set to True (quantify-scheduler!606).
- Compilation - Move `ModulationFrequencies` to `CompilationConfig.hardware_options` (quantify-scheduler!660)

## Quantify-core

### Merged MRs (highlights)

- QCoDeS - Add qcodes-loop as dependency to ensure `InstrumentMonitor` runs correctly (quantify-core!452)
- Analysis - Allow adding additional arguments to `create_figures()` method of classes that inherit from `BaseAnalysis` (quantify-core#364, quantify-core!454)
- Visualization - `set_xlabel`, `set_ylabel` and `set_cbarlabel` now add an offset to the values displayed on the tick labels, if that is needed to properly display small ranges with a large offset (quantify-core!450, quantify-core#165)

## Requested discussions / Any other business

### @slavoutich: voltage biasing support in Qblox

> I am aware that there is work actively going in supporting voltage biasing of qubits, that will perhaps allow
> to remove a current source from racks. I want to check how it is going.

We're (at Qblox) actually not aware of work ongoing on this, we will report back should that change

### @tobiasbonsen: Quantify Day

- What: a day to look back on the history of Quantify, discuss what Quantify is/should be, determine our focus for the coming year, and enjoy a day out of the office together.
- When: Wednesday June 14th (full day)
- Preliminary agenda:
  - 9:00 Arrival + settling in
  - 9:30 presentation on history/background of Quantify by Adriaan
  - 10:00 Miro board with ideas on future development/focus: What should be done in the coming year?
  - 10:30 Short break
  - 10:45 Talking point 1
  - 11:30 Short break
  - 11:45 Talking point 2
  - 12:30 Lunch
  - 13:30 Talking point 3
  - 14:15 Short break - maintainers create initial timeline/priority draft from Miro board
  - 14:30 Open discussion: what have we missed? (look at Miro board)
  - 15:00 Summarizing: finalizing timeline/priorities draft
  - 15:30 Closing remarks
  - 15:40 Drinks + bites

### Adam: better name for AmplitudeReference

ReferenceScale

PulseMagnitude

MagnitudeReference

(Winner is (after the meeting): ReferenceMagnitude, see quantify-scheduler!652)


### StitchedPulse 

Presentation by Thomas on newly introduced StitchedPulse and StitchedPulseBuilder

Follow-up:
- Qblox going to add a message: after compilation, with this schedule, you are using this much of your waveform or instruction memory of qrm/qcm

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

*(empty)*


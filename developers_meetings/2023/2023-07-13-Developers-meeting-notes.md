## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Robert, Edgar, Daniel)

## Highlights

*(empty)*

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Documentation - New acquisitions tutorial (!694)

## Quantify-core

### Merged MRs (highlights)

- Deprecation - The `@deprecated` decorator now returns a function when decorating a function, instead of a class with a `__call__` method (!462).

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Requested discussions / Any other business

### Recovering the state of the setup after the experiment (aka Suggestions for how the measurement control handles QCodes parameters)

> @slavoutich @adamorangeqs @Timo_van_Abswoude
>
> Follow-up from internal dicsussions in OQS: when we sweep a parameter
> in measurement control (for example flux bias current), after the experiment
> it will be set to the last settable. More user-friendly behaviour would be
> to restore it. Is it a good improvement?

> Discussion:
>
> MC doesn't reset the system to whatever it was before 
>
> Update .run method such that pre-state gets re-set after running
> 
> Allow turning this off (for the case that a settable is not gettable/queryable)
>
> Default on is fine 


### Make handling acquisition channels more user friendly

> @adamorangeqs
>
> Something that was initial brought up quite a longer time ago and put on hold, and I want to see if we are able to make some progress on it again now.

> Discussion:
>
> When working with multiple qubits, they need to have different acq channels, and have to be consecutive 
>
> Idea: handled automatically, define the acq channels automatically based for the provided qubits
>
> Gabor: acq channel is defined from device configuration  
> Probably not a small change if you want to do it automatically  
> How do you know which channel was assigned to which qubit  
> Then dataset also needs to contain the qubit name  
>
> Can use strings, so could use qubit name as channel? 
> 
> How about multiple channels per qubit, then qubit + port clock?
> 
> Arbitrary channel name (or port clock as default?) is possible, mapping in metadata
>
> Outcome:
> 
> Adam will create ticket and add to https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow**  epic: https://gitlab.com/quantify-os/quantify-scheduler/-/issues/282#note_1471704159
>
> Adam requests overview of current state

## Pending topics (for next DMs)

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset

### Overview of current state of Acquisition redesign and work remaining

> @ereehuis promises to provide this upon new RawDataset implemented in quantify-scheduler  
> Upcoming first milestone: set of agreed upon requirements for RawDataset and ProcessedDataset by August 1st


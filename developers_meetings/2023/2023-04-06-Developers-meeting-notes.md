## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize hardware configuration** (spearheaded by: Tobias)
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Robert, Edgar, Daniel)
  - New Tutorial: Running an Experiment
    - "connecting sched & core" "what is possible with Quantify" showcase depicting T1 experiment on Qblox hardware :hourglass:
    - Preview: https://quantify-quantify-scheduler--645.com.readthedocs.build/en/645/tutorials/Running%20an%20Experiment.html

- **Parallel work**:
  1. Clean up open tickets (~~235~~ **232** open [board](https://gitlab.com/groups/quantify-os/-/boards)) :hourglass:


## Highlights

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Qblox backend - Forbid repeated acquisition index in schedule (!655, !657, partially revert !542)
- Documentation - Acquisition data format in user guide (!646)
- ... and some minor maintenance MRs

## Quantify-core

### Merged MRs (highlights)

*(empty)*

## Requested discussions / Any other business

*(empty)*

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

*(empty)*


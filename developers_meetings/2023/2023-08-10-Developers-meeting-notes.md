## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)

## Highlights

*(empty)*

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Qblox backend - `rel_tol` must be greater than zero  (quantify-scheduler!752)
- Qblox backend - Extend `to_grid_time` to check for integer multiple of 1ns  (quantify-scheduler!751)
- Qblox backend: make acquire_append and acquire_average methods private (SE-(quantify-scheduler!219))  (quantify-scheduler!739)
- Fix repr collisions  (quantify-scheduler!738) 
- Fixes for Acquistions tutorial  (quantify-scheduler!732)
- Silence pyright  (quantify-scheduler!745)
- Fix time array in interpolated complex waveform  (quantify-scheduler!710)
- Docs - Fix broken list bullets on Read-the-Docs by enforcing `sphinx-rtd-theme>=1.2.2`  (quantify-scheduler!743)

## Quantify-core

### Merged MRs (highlights)

- Fix intersphinx links and broken refs (quantify-core!469)

## Review open (external) MRs

- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Requested discussions / Any other business

### communicating fixing of failing pipelines on main

@rsokolewicz - sometimes a pipeline fails not because of an error in the code, but because a new version of one of Quantify's dependencies came out and breaks something. We're usually quite fast with fixing this, but we don't always communicate to all developers working on merge requests that they should pull/merge/rebase main into their branch to get things working again. I sometimes post a message about this in this channel, but that quickly gets overlooked if you don't work on your merge request the same day. We could set up a bot to post a message in every open merge request to inform what is going on.

expected outcome of the discussion: identify if this is a real issue, and if so, a proposal how to improve communication.

Outcome/follow-ups:

- Instruction for all developers: When you encounter something / pipeline breaks unrelated to your changes, don't immediately start fixing but post message developers channel

- Add a checkbox to MR template whether to post message in developers channel that people should merge main back in e.g.

  - [ ] Pipeline fix: post in #software-for-developers channel to merge `main` back in (or N/A).

### modifying the signature of compiler node functions

@gdenes - I have an other requested discussion: I'm planning to change the signature to the individual compilation node compile functions. The proposal: these functions would not have any return value, but the schedule argument is changed. And because sometimes we do not change, but rather create a new schedule in the compilation pass, the node's compile function's argument would be a reference to a schedule. That way everything can be done clearly.

Discussion:

- Robert: why not modify in place but also return the schedule
  - When passing a param by reference and also returning that param, it is unclear that the passed by ref param itself may have been changed 

- Gabor: we can also not just simply remove the return value, as some nodes need to be able to update the schedule reference to point to a different object
  - Proposes a wrapper object in which the schedule reference is contained, such that one can update it

Outcome: 

- Gabor will create a quick MR to show the proposed changes, we can then review that and decide how to move on
- Note that for the performance work, improving this interface is not strictly required


## Pending topics (for next DMs)

### Presentation on newly introduced ThresholdedAcquisition

> @rsokolewicz will present after holidays so we have larger audience

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset

### Overview of current state of Acquisition redesign and work remaining

> @ereehuis promises to provide this upon new RawDataset implemented in quantify-scheduler
> Upcoming first milestone: set of agreed upon requirements for RawDataset and ProcessedDataset by August 1st

## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Robert, Edgar, Daniel)

- **Parallel work**:
  1. Clean up open tickets (~~199~~ **201** open [board](https://gitlab.com/groups/quantify-os/-/boards)) :hourglass:
  2. Make working with docuemntation convenient.

## Highlights

*(empty)*

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Qblox backend - Add `MarkerPulse` to pulse library, and implement Qblox backend compilation for this pulse (quantify-scheduler!628)
- Qblox backend - The compiled offset value in `AwgOffsetStrategy` is adjusted to match the changes to pulse amplitudes in quantify-scheduler!652. A pulse with a given amplitude `A` and a `VoltageOffset` with offset `A` will now produce the same voltage at the hardware level (quantify-scheduler!683).
- Qblox backend - Fix bug where LO/IF frequency `nan` is treated as overconstrained mixer (quantify-scheduler!690, quantify-scheduler#423)

## Quantify-core

### Merged MRs (highlights)

- MeasurementControl - Add `get_idn()` method, without it will generate warnings in using recent versions of QCoDeS (quantify-core!459)

## Requested discussions / Any other business

### @djweigand: Use None instead of float("nan") if scalar values are not set

I would like to add https://gitlab.com/quantify-os/quantify-scheduler/-/issues/424 to the agenda tomorrow, as it would be a change in the way of working. I propose using only None instead of nan for scalars (using nan in an array is fine and should be supported).

Follow-ups:
- On pydantic level, we should santize `nan` to `None`
- Note to all maintainers to make sure this adhered to for scalar values, using None instead of `nan`
- Note to @tobiasbonsen to take this into account in Compilation Config work


### @djweigand: Need for adding type hints to DeviceElement

- Lots of classes in need of type hints for qcodes param
- Daniel will create a ticket and mark it `Good first issue`


### Note on `__init__` docstrings: do not use `__init__` docstrings!

- No `__init__` docstring at all please, instead use the class docstring


## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests


## Pending topics (for next DMs)

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset


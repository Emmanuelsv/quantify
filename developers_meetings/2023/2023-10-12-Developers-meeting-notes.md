## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)

## Highlights

*(empty)*

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Qblox backend - Prevent repeated port-clock combinations (quantify-scheduler!799)
- Compilation - Enforce always adding (possibly empty) `HardwareOptions` to the `HardwareCompilationConfig` (quantify-scheduler!812)
- Compilation - Hotfix for quantify-scheduler!812: Fix backwards compatibility old-style hardware config for custom backend (quantify-scheduler!818)
- Operations - Adjust pulse info and acquisition info definitions to take class name (quantify-scheduler!809)
- Operations - Introduce the `GaussPulse`, an operation equivalent to the `DRAGPulse` but with zero derived amplitude, as well as its factory function `rxy_gauss_pulse`. (quantify-scheduler!793)
- Documentation - Update the deprecated code suggestions table (quantify-scheduler!815)
- Deprecation - The deprecated `instruction_generated_pulses_enabled` Qblox hardware configuration parameter, and the two classes related to it (`StitchedSquarePulseStrategy` and `StaircasePulseStrategy`) have been removed. The deprecated code suggestions have been updated (quantify-scheduler!811).
- Schedule - Added looping: An inner schedule can be repeated inside of the schedule (quantify-scheduler!709, quantify-scheduler!819).
- QuantumDevice - `ManualParameter` `elements` and `edges` have been changed from `list` to `dict` (quantify-scheduler!813)
  - Before, these were lists with instrument names
  - Now, these are dicts with instrument names as keys and and the `DeviceElement` and `Edge` instances as values
  - Not a breaking change however as `qcodes` already presented these `ManualParameter` as dicts with instrument names mapping to the instrument, also see [Tutorial: Operations and Qubits - Device configuration](https://quantify-os.org/docs/quantify-scheduler/latest/tutorials/Operations%20and%20Qubits.html#device-configuration)

## Quantify-core

### Merged MRs (highlights)

*(empty)*

## Review open (external) MRs

- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Requested discussions / Any other business

### Schedule functions with the new backend features

> @djweigand: I would like to update the schedule functions to use shiny new features (e.g. control flow), but the ZI backend does not support them. This issue will become worse, and I think it would be bad if the schedule functions only support the lowest common denominator of all backends. I think we need a "reverse deprecation policy" (can't think of a better name): x time after releasing a new feature, we are allowed to use it in the shared schedule functions, whether the backend supports them or not. My suggestion so that we have good deprecation policy but users get the new stuff in a speedy manner:
> - new backend-independent feature gets added (version x)
> - version x: add a legacy flag to schedule function, by default True . This uses the old function
> - version x+1: set default to False , add deprecation warning for True
> - version x+4: remove legacy code, ignore legacy argument such that there is no breaking change for users using the new version already

## Pending topics (for next DMs)

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset

### Overview of current state of Acquisition redesign and work remaining

> @ereehuis promises to provide this upon new RawDataset implemented in quantify-scheduler
> Upcoming first milestone: set of agreed upon requirements for RawDataset and ProcessedDataset by August 1st

### Reorganization of schedule library

> @ereehuis, @djweigand


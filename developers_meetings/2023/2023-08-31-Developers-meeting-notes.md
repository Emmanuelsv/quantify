## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)

## Highlights

*(empty)*

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Infrastructure - Use new qcodes syntax for adding parameters. (quantify-scheduler!758)
- Documentation - Fix missing images in Jupyter cell outputs in documentation deployed using Gitlab Pages. (quantify-scheduler!772, quantify-scheduler#404, counterpart of quantify-core!480)
- Gettable - Add `ScheduleGettable.initialize_and_get_with_report` that saves information from an experiment in a report zipfile for diagnostics. (quantify-scheduler!672)
- Qblox backend - Remove code referencing RF pulsars (these devices do not exist) (quantify-scheduler!748).
- Documentation - Explain in Qblox Cluster docs the possibility of using `"{complex,real}_output_<n>"` hardware config keys for both playback and acquisitions. (quantify-scheduler!763)
- Infrastructure - Add `jinja2` as dependency to quantify-scheduler (needed for `pandas.DataFrame`) (quantify-scheduler!777)
- Documentation - Split source and build folders to simplify using [`sphinx-autobuild`](https://github.com/executablebooks/sphinx-autobuild) for its editing. (quantify-scheduler!774)

## Quantify-core

### Merged MRs (highlights)

- Documentation - Fix missing images in Jupyter cell outputs in documentation deployed using Gitlab Pages. (quantify-core!480, counterpart of quantify-scheduler!772)
- :tada: Measurement Control - Improve progress bar for iterative and batch run with tqdm. (quantify-core!477, quantify-core#346)
- Documentation - Switch to `pydata_sphinx_theme` for documentation (quantify-core!479)

## Review open (external) MRs

- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Requested discussions / Any other business

### Demo on schedule compilation performance

> @gdenes

Where to put performance notebook: in developer guide on core https://quantify-quantify-core.readthedocs-hosted.com/en/latest/dev/index.html

For automated setup, options for the reports:
- GitLab metrics https://docs.gitlab.com/ee/ci/testing/metrics_reports.html
- Grafana https://grafana.com/

### Presentation on newly introduced ThresholdedAcquisition

> @rsokolewicz will present after holidays so we have larger audience

### Towards maintainable documentation

> @slavoutich: I would like to give a small demo of how to maintain documentation and not waste too much time on it.

## Pending topics (for next DMs)


### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset

### Overview of current state of Acquisition redesign and work remaining

> @ereehuis promises to provide this upon new RawDataset implemented in quantify-scheduler
> Upcoming first milestone: set of agreed upon requirements for RawDataset and ProcessedDataset by August 1st

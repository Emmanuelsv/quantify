## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)

## Highlights

`quantify-scheduler-0.16.1` has been released to fix a bug in acquisition in Qblox backend.

Documentation has officially moved to quantify-os.org domain ([quantify-core](https://quantify-os.org/docs/quantify-core/), [quantify-scheduler](https://quantify-os.org/docs/quantify-scheduler/)).
ReadTheDocs is disabled and set to forward to the new docs location.

### Breaking changes

- Acquisition - ~~`WeightedIntegratedComplex` and `NumericalWeightedIntegratedComplex` now return normalized integrations, similar to `SSBIntegrationComplex` (quantify-scheduler!801)~~ Not anymore. (quantify-scheduler!806, quantify-scheduler#449)

## Quantify-scheduler

### Merged MRs (highlights)

- Qblox backend - Fix missing signal on O2 and O4 outputs of baseband modules. (quantify-scheduler!803)
- Acquisition - Revert quantify-scheduler!801 (`WeightedIntegratedComplex` and `NumericalWeightedIntegratedComplex` now return normalized integrations, similar to `SSBIntegrationComplex`). (quantify-scheduler!806, quantify-scheduler#449)
- CI - Configuration of the CI pipeline has been completely moved to a separate repo, so that it can be managed for all branches at once. (quantify-scheduler!807)
- Documentation - Changed all documentation references from ReadTheDocs domain to quantify-os.org domain. (quantify-scheduler!798)

## Quantify-core

### Merged MRs (highlights)

- CI - Configuration of the CI pipeline has been completely moved to a separate repo, so that it can be managed for all branches at once. (quantify-core!496)
- Documentation - Changed all documentation references from ReadTheDocs domain to quantify-os.org domain. (quantify-scheduler!492)

## Review open (external) MRs

- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Requested discussions / Any other business

### Weighted acquisition is not normalized w.r.t. acquisition duration

https://gitlab.com/quantify-os/quantify-scheduler/-/issues/449

Slava: Best way to fix is within instrument coordinator

Daniel: No, you need to inspect the schedule if you want to normalize. User can easily do this themselves

Edgar: This will not be worked on for now by Qblox SE

### Deprecation: add to Deprecated code suggestions table

https://quantify-os.org/docs/quantify-scheduler/latest/examples/deprecated.html

Should be done upon introducing deprecation

Will adjust the deprecation MR checkbox to reflect this


### Problem with weak references and garbage collection in `QuantumDevice`

> @tobiasbonsen: I'd like to discuss https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/813 in tomorrows develoipers meeting. It fixes the garbage collection of unreferenced quantum device elements. The question is why the original quantum_device.elements was implemented as a QCoDeS parameter containing a list of element names in the first place, and if we should replace it by an attribute that contains a list/dict with references to the device elements.

> @ereehuis: For the discussion, I think it had to do with the fact that you get an overview of the keys, see
> https://quantify-os.org/docs/quantify-scheduler/latest/tutorials/Operations%20and%20Qubits.html#device-configuration
> ```python
> print(list(device_cfg.elements))
> print(list(device_cfg.edges))
> print(list(device_cfg.clocks))
>
> ['q0', 'q1']
> ['q0_q1']
> ['q0.01', 'q0.ro', 'q1.01', 'q1.ro']
> ```

Why elements names not actual elements? For snapshots, otherwise things get duplicated in snapshot
 
Just replace by dictionary?

Manual parameter that will be a dict

Do the same for edges

Would the dictonary work with the snapshot: we'll have to try

There is a qcodes dictionary parameter


### CompiledSchedule.timing_table will break soon with new pandas version

https://gitlab.com/quantify-os/quantify-scheduler/-/issues/450

We can either look into it now or when it actually breaks

Timo will take a look


### Plotmon keeps crashing

> Edgar, on behalf of Anuj

This closes both main and secondary 2D plotmon windows:
An existing connection was forcibly closed by remote host

Hiresh: Use plotmon all the time, don't have this issue

Thomas: Unstable connection? 

Slava: Is plotmon reading the files from network/remote file system?

Perhaps too many requests by Windows

Change from 1s plotmon interval to 10s interval


### Inconsistency in time domain schedules

https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/805

> Daniel, Nicholas

docstring and schedules do not agree

they should consistent echo ramsey dynamical decoupling schedules

take away: try to find a new article reference, probably we ignore dynamical decoupling

throw an error when gates overlap


## Pending topics (for next DMs)

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset

### Overview of current state of Acquisition redesign and work remaining

> @ereehuis promises to provide this upon new RawDataset implemented in quantify-scheduler
> Upcoming first milestone: set of agreed upon requirements for RawDataset and ProcessedDataset by August 1st

### Reorganization of schedule library

> @ereehuis, @djweigand


## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Robert, Edgar, Daniel)

- The end of &5 is approaching, and we are eagerly awaiting a list of requirements that will be utilized for reviewing the associated merge requests. Once obtained, these requirements will be made available in a public location, such as GitLab.

- Regarding &1, it is almost complete with only two minor merge requests remaining. Additionally, some documentation needs to be written, and the entire project should undergo hardware testing. However, there is a concern regarding the hardware options not appearing in a snapshot or the instrument monitor. We need to determine and formalize whether and how the snapshot should include more than just instrument parameters. It might be possible to expand the snapshot provided by quantify-core to store additional information.

- As for &2, it is also nearing completion. We are currently working on creating two new tutorials: one focusing on acquisitions and the other on schedulegettable (!685).

- **Parallel work**:
  1. Clean up open tickets (~~215~~ **199** open [board](https://gitlab.com/groups/quantify-os/-/boards)) :hourglass:

## Highlights

- Regarding `ReferenceMagnitude`, Daniel will handle the compilation aspect, although it is not currently considered a high priority. Adam has managed to create a functional implementation for Orange Quantum, but it would be preferable to have a proper implementation within the quantify framework. Additionally, more documentation on `ReferenceMagnitude` should be developed. We have identified a bug where it is incompatible with `StitchedPulse`, but a fix is already in progress. It is suggested that the `ReferenceMagnitude` compilation be a separate node, although this may potentially introduce performance issues.

- There is dissatisfaction regarding the inclusion of graphviz (quantify-scheduler!653) as a dependency in quantify. It was added to the `quantify-scheduler[docs]` requirements in order to include a notebook in the read-the-docs documentation. This notebook contains code snippets demonstrating how to conveniently generate UML class diagrams, which can be beneficial for developers. However, the inclusion of `graphviz` can lead to challenging dependency conflicts and is difficult to maintain. Possible options to address this concern are: (1) completely remove the dependency, or (2) implement lazy-loading, utilizing `graphviz` only if it is installed (e.g., on readthedocs.org).

- The Quantify day has been rescheduled to Wednesday, June 21, due to a scheduling conflict with quantum-week activities.

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Compilation - Introduced new `ReferenceMagnitude` parameter for pulses to allow specification of amplitudes and powers over different orders of magnitude (using variable attenuations, for example) (quantify-scheduler!652, quantify-scheduler!684)
  - Currently, only the user interface is implemented - changes to the backends will be made later (quantify-scheduler#413)
  - The code is backwards compatible, i.e., all schedules remain valid, but pulse amplitudes are not backwards compatible and will need adjustment / re-calibrating
  - If `ReferenceMagnitude` is set, `RuntimeWarning` is emitted.
- Qblox backend - Lowering the minimum time between acquisitions to 300 ns (quantify-scheduler!676, quantify-scheduler#369)
- Operations - The function `convert_to_numerical_pulse` has been added, which can be used to convert `StitchedPulse` to `NumericalPulse` (quantify-scheduler!665).
- Documentation - Fix double graph compile figure and some polish" (quantify-scheduler!681)
- Documentation - Add `UML_class_diagrams` notebook for visualizing class hierarchies (quantify-scheduler!653)
  - N.B. This MR introduces Graphviz as a dependency, which is very undesirable and we are thinking how to adjust it now.
- Gettables - When `ScheduleGettable.get()` is called and the associated `InstrumentCoordinator` returns nothing (for example, if the hardware configuration was never set), this will no longer raise a KeyError from the xarray module. Instead, a more helpful error is raised before the data processing (quantify-scheduler!671).
- Compilation - Support batched frequencies in schedule resources (quantify-scheduler!670)
- Compilation - Move `PowerScaling` (gain/attenuation) to `CompilationConfig.hardware_options` (quantify-scheduler!673)
- Qblox backend - Compilation uses `math.isclose` instead of `numpy.isclose` in certain cases to improve compile time (quantify-scheduler!682)

## Quantify-core

### Merged MRs (highlights)

*(empty)*

## Requested discussions / Any other business

Currently, it is not possible to incorporate reference operators and timing constraints for multi-qubit gates in a schedule within Quantify. The current implementation of the `Schedule` only allows a single `Schedulable` to be passed to the `ref_op` function. However, for a 2-qubit gate like the CZ gate, two gates need to be referenced. Moreover, the absolute timings used in Quantify are incompatible with multi-qubit gates. Attempting to add two absolute delays relative to two reference gates could lead to issues. To address this limitation and achieve full support for multi-qubit gates, a two-part solution is required:

1. Introducing flexible timing options, allowing the specification of time-delays using expressions such as ">10ns" or "<50ns" in addition to exact delays.
2. Modifying the process of adding operations to a `Schedule` to allow for multiple reference operations and timing constraints.

An obvious use-case for these changes is the CZ gate. These two parts can be developed as separate merge requests.

Daniel may take the lead on this initiative, but currently has higher priority tasks in the short term.

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

- There are several long-standing merge requests that are connected to a new API outline. However, the current implementation generates numerous warnings during the documentation build and proves challenging to maintain. It is worth considering migrating the documentation from Sphinx to a different tool like MakeDocs, for instance, to address these issues.

- The development of the Marker pulse feature is progressing slowly but remains an active project.

- The merge request for sudden-net-zero (quantify-scheduler!581) is on the verge of resolution and will be merged very soon. A successful demonstration was conducted this week, and the only remaining obstacle is the finalization of a few tests.

- Support for a keyside backend will not be included in quantify, and as a result, quantify-scheduler!425 will be closed.

## Pending topics (for next DMs)

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset


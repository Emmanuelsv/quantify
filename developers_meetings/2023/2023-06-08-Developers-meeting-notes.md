## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Robert, Edgar, Daniel)

- **Parallel work**:
  1. Clean up open tickets (~~201~~ **205** open [board](https://gitlab.com/groups/quantify-os/-/boards)) :hourglass:
  2. Make working with docuemntation convenient.

## Highlights

`quantify-scheduler-0.14.0` is released, with many updates to compilation and Qblox backend. Notable change is an introduction of `ReferenceMagnitude` parameter to allow easier control of attenuation/gain in the compiler. However, this is not yet really implemented by backends. See [changelog](https://gitlab.com/quantify-os/quantify-scheduler/-/blob/main/CHANGELOG.md#0140-2023-06-02) for more details.

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Qblox backend - Fix bug where LO/IF frequency `nan` is treated as overconstrained mixer (quantify-scheduler!690, quantify-scheduler#423)
- Schedules - Remove one of the `CRCount` operations in `nv_dark_esr_sched_nco` from NCO frequency loop to avoid redundancy (quantify-scheduler!643)
- Compilation - Changed units of amplitude parameters in device elements to dimesionless, for consistency with new `ReferenceMagnitude` interface (quantify-scheduler!691).

## Quantify-core

### Merged MRs (highlights)

- Documentation - Remove `jupyter-sphinx` extension and port snippets formerly served by it into a How-To section in the documentation. (quantify-core!460)

## Requested discussions / Any other business

### Compensation for propagation delay phase

> @adamorangeqs: Hi all, I would like to propose an agenda point for tomorrow's developers meeting
> I would like to discuss how to handle the parameter sequencer.nco_prop_delay_comp_en in quantify. This parameter enables a mode which compensates for the propagation delay phase. When this mode is not enabled, you can get a large oscillating background signal on your QRM measurements. We had some trouble in the lab this week because this mode was not enabled by default and we did not know why we were seeing this background signal. I would like to discuss the possibility of enabling this mode by default, either in quantify or in the Qblox driver.
>
> @djweigand: We had some lengthy internal discussions for the side of the qblox driver, I think the conclusion was to leave it disabled there, especially because it comes with caveats. On the quantify side this is certainly possible though.
>
> @jvoven: we are about to release an update that eliminates the background signals without the need for delay compensation. Ask Adith for more details.


Discussion:

Daniel:
- Compiler knows when the last frequency changed happened, so we can do it in Quantify
- What's going on:
  - Oscillations are caused by underflow during demodulation
    - Using a too small number of bits of your ADC
  - Working on a better way of doing demodulation (in qblox-instruments, what Jules mentioned)
    - This will get rid of the oscillations aka the quantization noise fix
  - Also, in (properly) setting nco propagation delay, oscillation is also gone

tl;dr 
- For now, best approach is to indeed enable nco propagation delay
  - Default settings seem to help already, here is a tutorial on how to set it more accurately: this is risky business though, if you set it wrongly results go haywire https://qblox-qblox-instruments.readthedocs-hosted.com/en/master/tutorials/q1asm_tutorials/rabi_experiment.html#TOF-calibration
- First to be released is the quantization noise fix (via qblox-instruments and quantify-sched updates)
- In parallel, we will work on enabling and possibly user settable nco propagation delay in quantify-scheduler


### Qiskit and Quantify integration

> @djweigand: Axel Andersson is ready to take over stewardship of https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/465 with the goal of merging, and would like to align

Current state:
- Not covered by tests
- Better user interface required?
- Documentation missing
- Essentially: transpilation between qiskit open pulse and quantify scheduler
- @kel85uk was involved in the design

Discussion:
- Instead of OpenPulse, go through OpenQASM (which is intended as intermediate representation), so make a frontend in quantify for that IR? Not for now

- Axel to talk to Kelvin
  - Essentially, to answer: should we add tests, better UI, docs, or, 
  - should we use as inspiration and create nice lightweight, as much isolated from other parts of quantify as possible, implementation??

### Missing Hadamard gate

> @djweigand: Rohit has a brief discussion about the missing Hadamard gate https://gitlab.com/quantify-os/quantify-scheduler/-/issues/290

Discussion:
- Hadamard requires being able to do composite gate
- Composite gates not possible in quantify-scheduler
- For error correction, logical gates, we would def need composite gates
- Requires extra compilation step that would decompose gates into fundamental gates
- Qiskit can do gate decomposition 

Conclusion: we will add RZ gate instead which is also missing and needed for Hadamard 

Gate decomposition we will probably facilitate in a future via Qiskit integration :fingers_crossed:


## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset


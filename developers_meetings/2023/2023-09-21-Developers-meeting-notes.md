## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)

## Highlights

Schedule composition and repetition loops initial support has been landed!

### Breaking changes

- Qblox backend - `QuantifyCompiler.compile`, `determine_absolute_timing` and `compile_circuit_to_device` restandardized: returned schedule is the transformed schedule; and added `keep_original_schedule` argument (quantify-scheduler!771)

## Quantify-scheduler

### Merged MRs (highlights)

- Gettable - For Qblox instruments, add serial numbers and version info (via `get_idn` and `_get_mods_info`) to the report zipfile for diagnostics. (quantify-scheduler!787)
- Qblox backend - Remove unnecessary deepcopies from schedule for 30-75% performance improvement (quantify-scheduler!771)
- Documentation - Trigger count documentation for acquisition protocols documentation (quantify-scheduler!780)
- Schedule - A schedule can now be added to another schedule. It will be treated as one big operation (quantify-scheduler!709).
- Schedule - Added looping: An inner schedule can be repeated inside of the schedule (quantify-scheduler!709).

## Quantify-core

### Merged MRs (highlights)

- SI utilities - Handle nan in value_precision input (quantify-core!487)
- Utilities - Create utility function to compare snapshots (quantify-core!485)
- Documentation - From now on documentation is automatically deployed to [quantify-os.org domain](https://quantify-os.org/docs/quantify-core/latest/) (quantify-core!484)

## Review open (external) MRs

- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Requested discussions / Any other business

### Plotting `NumericalPulse` bugfix MR

> @axean: a small MR related to a plotting bug  when plotting NumericalPulse that me and @rnavarathna found and implemented a fix for (quantify-scheduler!783).

### Update on Qiskit interface

> @axean: I would like to give some updates on my work on the Qiskit interface, since a "first version" is in a workable state.

## Pending topics (for next DMs)

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset

### Overview of current state of Acquisition redesign and work remaining

> @ereehuis promises to provide this upon new RawDataset implemented in quantify-scheduler
> Upcoming first milestone: set of agreed upon requirements for RawDataset and ProcessedDataset by August 1st

### Reorganization of schedule library

> @ereehuis, @djweigand


## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Robert, Edgar, Daniel)

## Highlights

*(empty)*

### Breaking changes

- Instrument coordinator - Dimension names of the datasets returned by the instrument coordinator have changed. If you used them explicitly in processing, you can fix that by extracting the names dynamically from the dataset. Exact names of the dimensions are not guaranteed to be stable in the future. (quantify-scheduler!608)

## Quantify-scheduler

### Merged MRs (highlights)

- Compilation - Minor refactor of `circuit_to_device` to be compatible with `numpy>=1.25` (quantify-scheduler!706)
- Schedulables - Raises a more readable error when the reference point is a `Schedulable` that is not in the `Schedule` (quantify-scheduler!707)
- Gettables - Shape of the data returned by instrument coordinator components for different acquision protocols is semi-formalized and validated in the code of `ScheduleGettable.retrieve_acquisition()`. Data returned by Qblox and ZI LabOne backends is adjusted accordingly. (quantify-scheduler!608)

## Quantify-core

### Merged MRs (highlights)

- Documentation - Add install instructions for macOS users (quantify-core!464)

## Requested discussions / Any other business

### Make SignalModeType ZI-scpecific

@v_palomar:

> Request to move class SignalModeType from quantify_scheduler/enums.py to the zhinst backend

### Possible change of hardware config structure

@v_palomar:

> Notify that the hardware configuration structure might have to be re-designed in the future because of changes in the qblox backend

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset


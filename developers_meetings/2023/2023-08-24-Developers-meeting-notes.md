## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)

## Highlights

- `quantify-core-0.7.3` is released. See [changelog](https://gitlab.com/quantify-os/quantify-core/-/blob/main/CHANGELOG.md?ref_type=heads#073-2023-08-17) for details.
- `quantify-scheduler-0.16.0` is released. See [changelog](https://gitlab.com/quantify-os/quantify-scheduler/-/blob/main/CHANGELOG.md?ref_type=heads#0160-2023-08-17) for details.

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Security - Add `check=True` flag to all subprocess calls (see also Ruff rule PLW1510). (quantify-scheduler!767)
- Compilation - Add a `scheduling_strategy` parameter to `QuantumDevice` and `DeviceCompilationConfig` classes to enable new strategies for calculating absolute timing in `compilation.determine_absolute_timing`  (quantify-scheduler!736)
- Qblox backend - Make QASM fields aligning optional, disable by default (quantify-scheduler!741)
- Infrastructure - Improve speed of `make_hash` (affecting `Schedule.add`) and some compilation steps (quantify-scheduler!770)

## Quantify-core

### Merged MRs (highlights)

- Documentation - Improve documentation build time and enable `sphinx-autobuild` (quantify-core!471)
- Deprecation - Explicit handling of np.arrays to avoid deprecated functionality when converting a dimensional array into scalar. (quantify-core!476)

## Review open (external) MRs

- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Requested discussions / Any other business

### Batched adaptive measurements MR

> @djweigand: And finally I'd like to highlight https://gitlab.com/quantify-os/quantify-core/-/merge_requests/467 for review, as Yuejie's internship ends soon. This MR offers a significant speedup for slow measurements like flux scans.

Grab a nice image for gallery, for future code examples overview: get a nice image from a dummy

### Data types in Quantify datasets

> @slavoutich: I believe that data types we use for storing data in datasets are unnecessarily heavy and perhaps we should introduce more compact conventions for them on the acquisition protocol. Example: in ThresholdedAcquisition protocol, we store 0 or 1 in int32 (on Windows) or int64 (on Linux), whereas int8 would be completely enough and datasets would be significantly smaller.

```
Daniel: qblox returns float because averaging may be smaller than 0
Need to be careful as what is returned may be determined by vendor

Adriaan: acq protocol should enforce the data type

How to get there: per acq protocol, discuss what should be the datatype

Idea: Slava create draft MR

We can do after acquisition dataset work
```

### Performance in Quantify

> @djweigand: And another point on behalf of @ereehuis and myself: We'd like to increase awareness for performance. Especially changes in scheduler have the potential of big performance impacts.


```
Compilation time columning + deepcopy speedup:

2000 operation schedule: 2s => 1s
even larger schedule: 9s => 3s

Where to put it: developer's guide: (discuss in maintainers)

why not a performance class
run it from a notebook for now

on integrating in CI/CD: see GitLab metrics https://docs.gitlab.com/ee/ci/testing/metrics_reports.html
```


### Interface for predistortions

> @djweigand: Could you please also add the discussions requested by @rnavarathna (interface of predistortion) and myself (quantify-scheduler#438)? Thanks!

```
hardware based pre-distortions

Slava: desire to have the same distortion corrections be able to apply to multiple vendors
Adriaan: not easily possible to do this ^

what info does the user provide:
- pre-distortion protocol
- pre-distortion values
- filter func no longer a function call but a protocol specification
- bool to run on hardware or virtual
```

Distortion corrections at the level of Acquistion protocols in the documentation

### On documentation for new features

> @adriaan: adding tutorials for everything is not efficient / handy for the user, often they just want a working example of how to run something

Consider adding code examples instead of tutorial (see https://quantify-quantify-core.readthedocs-hosted.com/en/latest/howto/index.html)

Add MR checkbox: documentation, new features



## Pending topics (for next DMs)

### Presentation on newly introduced ThresholdedAcquisition

> @rsokolewicz will present after holidays so we have larger audience

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset

### Overview of current state of Acquisition redesign and work remaining

> @ereehuis promises to provide this upon new RawDataset implemented in quantify-scheduler
> Upcoming first milestone: set of agreed upon requirements for RawDataset and ProcessedDataset by August 1st

## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Robert, Edgar, Daniel)

- **Parallel work**:
  1. Clean up open tickets (~~205~~ **205** open [board](https://gitlab.com/groups/quantify-os/-/boards)) :hourglass:
  2. Make working with docuemntation convenient.

## Highlights

*(empty)*

### Breaking changes

- Qblox backend - Compile `ShiftClockPhase` operation to `set_ph_delta` + `upd_param`, extending duration from 0 to 8 ns (quantify-scheduler!704, quantify-scheduler#432)

## Quantify-scheduler

### Merged MRs (highlights)

- Gate Library - Added `Rz`, `Z` and `Z90` gate to gate library, `BasicTransmonElement` and tested the new gates in `test_gate_library.py` (quantify-scheduler!697, quantify-scheduler#290)
- Qblox backend - Compile `ShiftClockPhase` operation to `set_ph_delta` + `upd_param`, extending duration from 0 to 8 ns (quantify-scheduler!704, quantify-scheduler#432)

## Quantify-core

### Merged MRs (highlights)

*(empty)*

## Requested discussions / Any other business

### Follow-ups from Quantify Day

> @djweigand: Will there be a developers meeting tomorrow? One of my conclusions from today is that there is a discussion to be had about intermediate steps going towards the new intermediate representation and redefinition of backends. The question is, do we want to do it properly (and wait for months) or do a quick intermediate step - faster, but that will mean more breaking changes.
>
>In particular I think we can cram the concepts of loops and arrays (relatively) easily into the current quantify schedule. The implementation I have in mind would be relatively straightforward to support with both qblox backend and laboneq. Pro would be that performance of both backends improves a lot (in specific cases), the con that we duct tape even more. Both pros and cons are large, so something to think about. And there is of course the issue of this taking away resources from somewhere else..

### @tobiasbonsen: init_offset_awg_path_X and init_gain_awg_path_X settings

> I'd like to discuss the `init_offset_awg_path_X` and `init_gain_awg_path_X` settings in the qblox backend: I want to understand the use case(s) for these settings such that I can put them in the right place in the `HardwareCompilationConfig`.

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset


# Epics (APSMM23 release)

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize hardware configuration** (spearheaded by: Hiresh/Tobias)
  - Tobias will come with an updated plan on what will be done first and what is aimed to finish before APSMM
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Daniel)
  - We aim to deliver a "connecting sched & core" "what is possible with Quantify" showcase depicting T1 experiment on Qblox hardware
    - Next to that, we are adding Qblox Quantify showcases for transmon, spin, NV centers to [Qblox RTD](https://qblox-qblox-instruments.readthedocs-hosted.com/en/master/)
  - We are going over the existing tutorials of quantify-scheduler and bringing them up-to-date/removing depr code
- **Parallel work**:
  1. [milestone 3](https://gitlab.com/groups/quantify-os/-/milestones/3)  Quantify Platform release aka The Website :tm: (remaining: website + RTD polish)
  1. Clean up open tickets (~~260~~ **249** open and counting [board](https://gitlab.com/groups/quantify-os/-/boards))


## Highlights

*(empty)*

### Breaking changes

- Acquisition - `InstrumentCoordinator.retrieve_acquisition` returns an `xarray.Dataset` (quantify-scheduler!550, quantify-scheduler#362)

## Quantify-scheduler

### Merged MRs (highlights)

- Acquisition - `InstrumentCoordinator.retrieve_acquisition` returns an `xarray.Dataset` (quantify-scheduler!550, quantify-scheduler#362)

## Quantify-core

### Merged MRs (highlights)

- Error handling when getting parameters in `load_settings_onto_instrument` (quantify-core!425)

## Requested discussions / Any other business

### `QuantumDevice` init order
https://gitlab.com/quantify-os/quantify-scheduler/-/issues/403

Adam: 
- `DeviceElement` useless without hardware config
- Hence, we always need to link to a `QuantumDevice` upon creating a `DeviceElement`  

Daniel:
- Idea for no-breaking change: add option to `QuantumDevice.add_element` of creating and returning new `DeviceElement`


## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

*(empty)*


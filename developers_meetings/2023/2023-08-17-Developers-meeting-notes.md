## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)

On compilation config / connectivity:

-  same portclock on input and output not allowed
-  for clarity, we'd want to explicitly enforce using separate input and output, so needing ability to use same port clock (transmon case)
- idea: new connectivity setup should enforce this

## Highlights

*(empty)*

### Breaking changes

- Qblox backend - Rename `is_within_grid_time ` helper function to `is_within_half_grid_time` (quantify-scheduler!753)
- Qblox backend - Strictly requires v0.11.x of the `qblox-instruments` package (quantify-scheduler!723)

## Quantify-scheduler

### Merged MRs (highlights)

- Qblox backend - Added warning if waveform playback or acquisition is interrupted by another waveform or acquisition, respectively (quantify-scheduler!744, quantify-scheduler#436)
- Qblox backend - Hotfix for quantify-scheduler!723: turn on channel map parameters associated with inputs for the cases of output io names (e.g. `complex_output_0`) defined in readout modules. (quantify-scheduler!760)
- Documentation - Restore Qblox documentation on using the old-style hardware config (quantify-scheduler!761)
- Documentation - Fix broken xarray display in docs (propagate from quantify-core!470) (quantify-scheduler!762)

## Quantify-core

### Merged MRs (highlights)

- Analysis - Added analysis class for qubit spectroscopy (quantify-core!463)
- Documentation - Fix broken xarray display in docs and resolve sphinx warnings (quantify-core!470)

## Review open (external) MRs

- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Requested discussions / Any other business


### Read the Docs => GitLab pages

Currently on GitLab pages build, images missing, could have to do with theme (OQS also using it and not having this issue)

Do we care about multiple versions

For local debugging best be as close as possible to actual doc render system, which is much better for GitLab pages than for Read the Docs

=> We will discuss in Maintainer's meeting

### Refactor compilation passes with reference to schedule

https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/757

wrapper only needed for creating compiledSchedule

no clear definition what a compilation pass is allowed to do

Problem: Never stated anywhere that you can or cannot do in place 

In place is faster

Outcome: Put ideas on MR

### Implicit timing constraints at `Schedule.add` level

> @djweigand: I'd like to refactor such that the timing constraint is always filled by Schedule.add when not explicitly specified (default case) instead of determine_absolute_timing . This seems to have no functional impact and makes the logic much clearer, but I'd like to check if there are edge cases I forgot where this is not possible.

fill in default timing constraints when we create the schedule?

problem when allowing to select between alap / asap

Outcome: Daniel to create draft MR

### Specifying timing contraints relatively to the root and the end of the schedule

> @djweigand: For subscheduling, it would be useful to have access to the root and ideally also the end of a schedule. As a similar discussion came up with as late as possible scheduling, I'd like to align.

https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/709

Need ability to refer to begin and end of schedule

How about: same behavior for subscheduling using anchor points (begin, mid, end), why does it not work

Outcome: Daniel to discuss with Adriaan / put ideas on MR

## Pending topics (for next DMs)

### Data types in Quantify datasets

> @slavoutich: I believe that data types we use for storing data in datasets are unnecessarily heavy and perhaps we should introduce more compact conventions for them on the acquisition protocol. Example: in ThresholdedAcquisition protocol, we store 0 or 1 in int32 (on Windows) or int64 (on Linux), whereas int8 would be completely enough and datasets would be significantly smaller.

### Presentation on newly introduced ThresholdedAcquisition

> @rsokolewicz will present after holidays so we have larger audience

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset

### Overview of current state of Acquisition redesign and work remaining

> @ereehuis promises to provide this upon new RawDataset implemented in quantify-scheduler
> Upcoming first milestone: set of agreed upon requirements for RawDataset and ProcessedDataset by August 1st

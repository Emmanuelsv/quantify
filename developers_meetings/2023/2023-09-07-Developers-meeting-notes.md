## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)

## Highlights

[Sneak peak on the planned documentation hosting place](https://quantify-os.org/docs/quantify-core/latest/) as a replacement for ReadTheDocs.

Discussion:

- Read the docs cancelled per 30-10-2023
- Need to port quantify-scheduler to GitLab pages
- For both, need to decide which versions to make available / list in the selector


### Breaking changes

- `quantify-scheduler` now requires strictly `pydantic>=2.0` (quantify-scheduler!768, quantify-scheduler!781)
- Data - The `long_name` and `name` attributes of dataset coordinates now contain information about the root instrument and submodules in addition to the settable (quantify-core!478)

## Quantify-scheduler

### Merged MRs (highlights)

- Documentation - Add a short explanation and examples of the `StitchedPulse` and `StitchedPulseBuilder` to the Schedules and Pulses tutorial. (quantify-scheduler!766)

## Quantify-core

### Merged MRs (highlights)

- Data - The `long_name` and `name` attributes of dataset coordinates now contain information about the root instrument and submodules in addition to the settable (quantify-core!478)

## Review open (external) MRs

- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Requested discussions / Any other business

### Another iteration of API documentation dicscussion

> We are still trying to find a good way to build API reference documentation.
> Previously we converged on `autosummary` approach, but that likely was a bad decision.
> We should exchange our thoughts and discuss possible alternatives
> (like [sphinx-autodoc2](https://pypi.org/project/sphinx-autodoc2/),
> [sphinx-autoapi2](https://pypi.org/project/sphinx-autoapi2/) or vanilla
> [sphinx-autoapi](https://pypi.org/project/sphinx-autoapi/)).

Discussion:
- autosummary not setting ourselves up for success
- myst-parser documentation has nice API ref setup: https://myst-parser.readthedocs.io/en/latest/apidocs/index.html
  - based on `sphinx-autodoc2`
  - Slava proposes that we try that first
- Edgar: Not sure what the priority is
- Slava: merging core and sched goes first, then we'll return to this

Daniel: Can `sphinx-autodoc2` deal with submodules?
Edgar: what does it look like currently with `autoapi`? 
- Example https://quantify-quantify-scheduler.readthedocs-hosted.com/en/latest/autoapi/quantify_scheduler/device_under_test/transmon_element/index.html
- Qcodes params listed but no param values listed


### On self-calibration routines for auto mixer calibration in Qblox 

Kelvin: 
- Will the parameters be exposed via quantify?
- Transparent way of inspecting the generated parameters?

Daniel:
- If we have qcodes parameters, those would be available anyway
- Don't see a reason to include in quantify
- If there is an interface then it will be in qblox-instruments


## Pending topics (for next DMs)

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset

### Overview of current state of Acquisition redesign and work remaining

> @ereehuis promises to provide this upon new RawDataset implemented in quantify-scheduler
> Upcoming first milestone: set of agreed upon requirements for RawDataset and ProcessedDataset by August 1st

### Reorganization of schedule library

> @ereehuis, @djweigand

### Presentation of Qiskit integration

> https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/764
> @axean


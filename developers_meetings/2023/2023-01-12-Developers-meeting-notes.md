## Epics (APSMM23 release)

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize hardware configuration** (spearheaded by: Edgar)
  - https://gitlab.com/quantify-os/quantify-scheduler/-/issues/332 Update CompilationConfig with high-level structure
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Daniel)
- **Parallel work**:
  1. [milestone 3](https://gitlab.com/groups/quantify-os/-/milestones/3)  Quantify Platform release aka The Website :tm: (remaining: website + RTD replacement)
  1. Remove deprecated code / usage
  1. Clean up open tickets (**247** open and counting [board](https://gitlab.com/groups/quantify-os/-/boards))
  1. Fast feedback in Quantify (first MRs) (Qblox)
  1. https://gitlab.com/quantify-os/quantify-scheduler/-/issues/320 ProfiledScheduleGettable needs refactoring (Qblox)
  1. https://gitlab.com/quantify-os/quantify-scheduler/-/compare/main...QAE-472-Draft-parallel-hardware-prepare-support (Qblox)
  1. ...

## Highlights

The most work right now is going into documentation improvements.

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Schedule Functions - make experiment-related schedule functions available in `quantify_scheduler.schedules` (quantify-scheduler!572)
- Qblox backend - support NV center-specific TriggerCount acquisition protocol in `QRMAcquisitionManager` (quantify-scheduler!556)
- Qblox Backend - Added method for gain configuration, overriding gain now raises ValueError (quantify-scheduler!533)

## Quantify-core

### Merged MRs (highlights)

*(empty)*

## Requested discussions / Any other business

### Formalizing how hardware parameters are updated inside schedules (aka parametrized schedules)

Discussion leader: @djweigand

Main point: we'll need two new concepts, to scale up to multiple qubits and to allow for quantum error correction
- classical logic (Daniel will update https://gitlab.com/groups/quantify-os/-/epics/3)
- and parametrized schedules (New epic, Daniel will populate https://gitlab.com/groups/quantify-os/-/epics/6)

Parametrized schedules could also be implemented via Qiskit
Let's investigate, but note, Qiskit is not complete yet, mapping it to hardware capabilities required hacks

### Weighted integrations
@TimVroomans showing interest in https://gitlab.com/quantify-os/quantify-scheduler/-/issues/295

Tim will check with @r3b1r7h on status of https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/410 and how to possibly get progress on this issue

Qblox in the form of @djweigand also showing interest in the topic / design, implementing it for Qblox hw too (actually, we already have a stub ticket tracking this work it turns out)


## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

*(empty)*


## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize compilation configuration** (spearheaded by: Tobias)
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Robert, Edgar, Daniel)

- **Parallel work**:
  1. Clean up open tickets (~~222~~ **215** open [board](https://gitlab.com/groups/quantify-os/-/boards)) :hourglass:

## Highlights

`quantify-scheduler-0.13.0` is released, with [a lot of improvements](https://gitlab.com/quantify-os/quantify-scheduler/-/blob/main/CHANGELOG.md#0130-2023-05-05).

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Deprecation - Deprecated code that had been scheduled to be removed after version 0.12 has been removed. The deprecated code suggestions have been updated (quantify-scheduler!667).
- Compilation - Move `MixerCorrections` to `CompilationConfig.hardware_options` (quantify-scheduler!669)
- Compilation - Introduced new `ReferenceMagnitude` parameter for pulses to allow specification of amplitudes and powers over different orders of magnitude (using variable attenuations, for example) (quantify-scheduler!652)
  - Currently, only the user interface is implemented - changes to the backends will be made later (quantify-scheduler#413)
  - The code is backwards compatible, i.e., all schedules remain valid, but pulse amplitudes are not backwards compatible and will need adjustment / re-calibrating

## Quantify-core

### Merged MRs (highlights)

*(empty)*

## Requested discussions / Any other business

### Adam: On setting repetition for schedules

Conclusions:
- Only signaling repetitions via `quantum_device.cfg_sched_repetitions` actually works
- Setting via `schedule_kwargs` override doesn't work right now, and people don't see the need for it
  - This is the current implementation indicating that:
    ```python
            sched = self.schedule_function(
            **self._evaluated_sched_kwargs,
            repetitions=self.quantum_device.cfg_sched_repetitions(),
        )
    ```
    https://gitlab.com/quantify-os/quantify-scheduler/blob/b229d01c3d26f2e4f5ae5e751d1a00283ccca0ec/quantify_scheduler/gettables.py#L177-180

Requirement: we (only) need high level param to set number of repetitions for multiple schedules

Idea: move repetitions to param on instrument coordinator, and out of the schedule
- Upside: will be taken along in both running schedule via IC and via ScheduleGettable
- Downside: its an experiment param that is not in the CompilationConfig (however, it is included in the snapshot via IC)

(Related MR though not in line with last conclusions: quantify-scheduler!668)


## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

### Requirements for raw and processed dataset

> @ereehuis will distill the requirements for the dataset / acq redesign from the available writings; important distinction:
> - requirements for the raw dataset
> - requirements for the processed dataset


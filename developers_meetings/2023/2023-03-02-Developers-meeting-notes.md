# Epics (APSMM23 release)

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize hardware configuration** (spearheaded by: Hiresh/Tobias)
  - Tobias will come with an updated plan on what will be done first and what is aimed to finish before APSMM
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Daniel)
  - We aim to deliver a "connecting sched & core" "what is possible with Quantify" showcase depicting T1 experiment on Qblox hardware
    - Next to that, we are adding Qblox Quantify showcases for transmon, spin, NV centers to [Qblox RTD](https://qblox-qblox-instruments.readthedocs-hosted.com/en/master/)
  - We are going over the existing tutorials of quantify-scheduler and bringing them up-to-date/removing depr code
- **Parallel work**:
  1. [milestone 3](https://gitlab.com/groups/quantify-os/-/milestones/3)  Quantify Platform release aka The Website :tm: (remaining: website + RTD polish)
  1. Clean up open tickets (~~260~~ **249** open and counting [board](https://gitlab.com/groups/quantify-os/-/boards))


## Highlights

- `quantify-scheduler-0.12.0` is released with support for fast spectroscopy, a.k.a. NCO frequency sweep in Qblox backend.

### Breaking changes

- `quantify-core` requires now `qcodes>=0.37.0`

## Quantify-scheduler

### Merged MRs (highlights)

- Schedules - Add fast NCO sweep schedules using `SetClockFrequency` (quantify-scheduler!542)
- Compilation - Update `CompilationConfig` with high-level structure (quantify-scheduler!603, quantify-scheduler#332)
- Qblox backend - Fix Qblox sync reset bug following quantify-scheduler!550 (quantify-scheduler!611)

## Quantify-core

### Merged MRs (highlights)

- `mk_trace_for_iq_shot` now renders nicely in documentation (quantify-core!429)
- Utilities - Fix bug where calling `without` to remove a key from a dict that did not have that key in the first place raises a `KeyError` (quantify-core!438)

## Requested discussions / Any other business

### Linters (@slavoutich)

Merge request quantify-scheduler!614 enables [`ruff`](https://beta.ruff.rs/docs/) linter and [`pyright`](https://github.com/microsoft/pyright) type checker. The aim is to increase code quality and replace Codacy in the long run.

The problem with it is that files need to be gradually ported over. The aim of the discussion is to explain consequences of that and discuss how to proceed forward with making code `ruff`-compliant.

Discussion:
- we need some description/links on contribution guidelines 

- how to properly install
- how to ignore

- Also set up for q-core

### Top-level imports (@rsokolewicz)

Discussion:
- Okay on adding toplevel classes to be imported directly from quantify-scheduler
  - But, careful review not to break hierarchy by adding too many classes there
- Allows for 
  ```python
  import quantify-scheduler as qs

  qs.MeasurementControl 
  ```
- Related, okay to do for tutorials:

  ```python
  import ... gate_library as gl
  import ... pulse_library as pl
  ```


## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

*(empty)*


## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize hardware configuration** (spearheaded by: Tobias)
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Robert, Edgar, Daniel)
  - New Tutorial: Running an Experiment
    - "connecting sched & core" "what is possible with Quantify" showcase depicting T1 experiment on Qblox hardware :hourglass:
    - Preview: https://quantify-quantify-scheduler--645.com.readthedocs.build/en/645/tutorials/Running%20an%20Experiment.html

- **Parallel work**:
  1. Clean up open tickets (~~235~~ **236** open [board](https://gitlab.com/groups/quantify-os/-/boards)) :hourglass:

Discussion:
- Maintainers will check from time to time whether Epics need to be extended with new ones and/or open Epics need to be pruned/closed
- Essentially, the Epics are here to align on central pieces of work within the Quantify development that we would like to stay aligned on and we therefore touch on weekly during the dev meeting
- Likely future epic: around classical logic / looping / subschedules


## Highlights

ZI LabOne backend has bin fixed in `main` (quantfiy-scheduler!623).

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Schedules - Add `nv_dark_esr_sched_nco` spectroscopy schedule using SetClockFrequency Operation to sweep the NCO frequency (quantify-scheduler!639)

## Quantify-core

### Merged MRs (highlights)

*(empty)*

## Requested discussions / Any other business

### On quantify-scheduler#413

Main outcome of discussion: there will be separate MRs:
- Introducing reference power on pulse level
- Implementing per backend

### On Qiskit backend

Daniel: is anyone currently working on Qiskit Quantify integration (follow-up to https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/465)
- Axel is a new Qblox employee who we intend to continue work on this (he already wrote a Qiskit Quantify integration)
- Hiresh from OQS is also working on this already -- so, good to know, we will need to align (Edgar: _perhaps, add Epic for it??_)

### On possibly renaming SSBIntegrationComplex
Daniel: not a fan of the name, can/should we rename?

Adriaan: 
- Commonly used name
- Also, we should be careful renaming the protocols due to interface breaking


### On storing experiment parameters
Daniel: should we introduce some new structure for this, allowing to quickly load settings

Can already be done, Daniel to talk to Adriaan

### On weighted integration
Tim: how is that going

We have open MR, in need of reviewing by Daniel


## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

*(empty)*


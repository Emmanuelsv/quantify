## Epics (APSMM23 release)

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize hardware configuration** (spearheaded by: Edgar)
  - https://gitlab.com/quantify-os/quantify-scheduler/-/issues/332 Update CompilationConfig with high-level structure
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Daniel)
- **Parallel work**:
  1. [milestone 3](https://gitlab.com/groups/quantify-os/-/milestones/3)  Quantify Platform release aka The Website :tm: (remaining: website + RTD replacement)
  1. Remove deprecated code / usage
  1. Clean up open tickets (**247** open and counting [board](https://gitlab.com/groups/quantify-os/-/boards))
  1. Fast feedback in Quantify (first MRs) (Qblox)
  1. https://gitlab.com/quantify-os/quantify-scheduler/-/issues/320 ProfiledScheduleGettable needs refactoring (Qblox)
  1. https://gitlab.com/quantify-os/quantify-scheduler/-/compare/main...QAE-472-Draft-parallel-hardware-prepare-support (Qblox)
  1. ...

## Highlights

- `quantify-core-0.6.5` and `quantify-scheduler-0.10.1` are released :rocket::rocket::rocket:
- We provide now an [example notebook](https://quantify-quantify-scheduler.readthedocs-hosted.com/en/latest/examples/deprecated.html) to assist users to migrate from deprecated functionality.

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Plotly pulse diagrams now display clock names (quantify-scheduler!547)
- Several NV center-related MRs (quantify-scheduler!527, quantify-scheduler!551, quantify-scheduler!530)

## Quantify-core

### Merged MRs (highlights)

*(empty)*

## Requested discussions / Any other business

*(empty)*

## Extra discussion

*(empty)*

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

### Acquisition index rename (@gdenes)

*(Pending because @adriaanrol requested to postpone this discussion because of unability to attend DM.)*

> I've a proposition, which I think we should do as part of the acquisition redesign.
> We should rename the `acq_index`, because it's very ambiguous from the perspective of Qblox. The acquisition index on qblox-instrument loosely corresponds to `acq_channel` in quantify, and bin on `qblox-instrument` loosely corresponds to `acq_index` in quantify. This is very confusing. I'm fine with label/coordinate/tag/etc., almost anything (not bin, because there's not always a one-to-one correspondance between the hardware bin).
https://gitlab.com/groups/quantify-os/-/epics/5#note_1169469747

### Formalizing how hardware parameters are updated inside schedules [organized by @djweigand and/or @damazdejong]



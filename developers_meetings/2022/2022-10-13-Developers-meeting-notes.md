## Highlights

- Quantify-Scheduler is at 0.9.0 (quantify-scheduler!475)
- Merged memory leak fix by Matteo Pompili (quantify-core!390, quantify-core!391)

### Announcement from @ereehuis
Related, tip for our users, to enable deprecation warnings in jupyter notebooks as there they are suppressed, please add the following code snippet in the first cell prior to importing quantify in your notebook(s):

```
import warnings

warnings.simplefilter("default")
```

### Breaking changes

## Quantify-scheduler

### Merged MRs (highlights)

## Quantify-core

### Merged MRs (highlights)
- Fix for memory leak issue (quantify-core!390, quantify-core!391)

## Requested discussions / Any other business

### Zhinst Parser error occurred during parsing of command table JSON loaded to AWG 3
- Martijn Zwanenburg running into:
  ```
  We have:

  zhinst                        21.8.20515
  zhinst-hdiq                   1.0.0
  zhinst-qcodes                 0.1.4
  zhinst-toolkit                0.1.5

  The error looks something like this:

  Error2022/9/5 10:59:09 41681363299603622 Parser error occurred during parsing of command table JSON loaded to AWG 3

  Two things I've noticed:
  - We never experienced this when we were using one channel of the AWG at the time. The error only seems to pop-up when you compile waveforms to at least two channels on the AWG.
  - Turning the AWG off and on resolves the error. If you run the exact same experiment with the exact same waveform after restarting the AWG, the error is no longer there.
  ```
- **Desired outcome**: Kelvin hoping that some other Zhinst users (@christiandickel8 et al, @gtaifu/@r3b1r7h) are able to chip in some wisdom
- Kelvin: Others encountering this too? 
- Zhihao: issue not familiar
- Kelvin: will create a proper issue then to follow up

### Issues with hardware config of Zhinst backend
- Tim Vroomans facing issues with configuring the zhinst backend for his own setup. Potentially a fix is discussed but not tested.
- Poor documentation of the zhinst backend led to confusion with the writing of the hardware config.
- Poor error message also led to such confusion.
- Tim: 
  - Needed HDAWG wasn't clear from the documentation or the error
  - Will create an issue for gathering more info and following up

### Extra discussion

- Adam: two MRs in need of review: quantify-core!388 quantify-core!389  
  @rsokolewicz will review

- External MR by Pieter in need of mirror MR and review: quantify-core!376  
  @gdenes will create mirror MR for fixing the pipeline (see steps here: https://gitlab.com/quantify-os/quantify-core/-/merge_requests/390#note_1126263056), @kel85uk available to do the actual merge

- Robert: Reset clock phase MR in need of testing on Zhinst hardware quantify-scheduler!434  
  @TimVroomans offers to test it, Robert will bring up to date with main first

- Adriaan: Clear MR titles are important (for reviewing among other things), plz pay attention to that

### Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

### Public API (No follow up yet since 2022-08-4) [Owner: @rsokolewicz (guidelines, listing)]
Also see https://quantify-hq.slack.com/archives/C02DE4ZENNQ/p1659630403947179?thread_ts=1659541477.414369&cid=C02DE4ZENNQ

We need to discuss about plans for making formal the public APIs so that we do not expose everything within Quantify
as a public API. This is so that any internal implementations would not break a dependent package. (quantify-scheduler#313)
- What is public (and thus breaking, in need of using deprecation scheme upon change), what is private
- Make whole modules private?

**Decision**:
- Whatever user needs is public.
- Guideline: "private, unless..."
- Process: minor changes incorporated to actual work.

### Line lengths (No follow up yet since 2022-08-4) [Owner: @damazdejong]
Requested by Damaz - A proposal to improve the way we work with Quantify styles.

Damaz: Codacy is complaining when Black fails to reformat a string to 88 chars.
Zhihao: Docstrings are not formatted at all.
**Decision**: Move the discussion online.

### Intermediate representation (No follow up yet since 2022-08-4) [Owner: @gtaifu]
Xiang: we are interested in discussion on intermediate representation, especially in the context of Quingo compiler
(https://dl.acm.org/doi/10.1145/3483528).
**Decision**: Xiang will make slides about Quingo and share them in Slack, later we will schedule a slot for presentation.


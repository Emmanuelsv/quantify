## Highlights

### Breaking changes

- Qblox backend now resets clock phase every measurement by default (quantify-scheduler!434)
- `quantify_core.visualization.SI_utilities.set_xlabel()` function has changed signature. (quantify-core!376)

## Quantify-scheduler

### Merged MRs (highlights)

- Writing pulse sequence file on disk can be disabled in Qblox backend (quantify-scheduler!438)
- Optimization/bugfix in Qblox acquisition manager (quantify-scheduler!478):
  - It is possible te set length of scope acquisition
  - It is possible to set different duration for each sequencer in acquisition.
- To avoid state-dependent behaviour inconsistencies in tests, all instrruments are closed at the start of every test.
  This, however, increases the duration of the test suite slightly (quantify-scheduler!492)

## Quantify-core

### Merged MRs (highlights)

- `load_settings_onto_instrument()` function (which loads settings from a specified TUID onto the current setup) now supports granular upload (onto a specific instrument, channel or parameter) (quantify-core!384:)

## Requested discussions / Any other business

### @gdenes: Removing the deprecated functionality

> We're going to have a small quantify-scheduler minor release in the following 1-2 weeks. I want to have a small
> discussion whether we can remove deprecated functionality, which we already said would be removed in previous
> versions, like 0.7.0.

Deprecated functionality should have been removed with the previous 0.9.0 release (oops). Although we mention deprecated functionality will be removed after e.g. 0.7.0, it is preferable to remove it in minor releases, not in patch releases. Users shouldn't expect patch releases to break any code. We decide remove these at 0.10.0. We will also update our written deprecation policy to make this clear. 

### Extra discussion

**hiresh**: in quantify-core!382 there is a function related to loading plot monitors from historical datasets which needs to be updated. However, there is a piece of code that should be updated but might be better to just remove from the codebase entirely. The piece of code relates to the ability to load multiple plot monitors from the same dataset. Because the code has been broken for 18 months and noone noticed we decide to just remove it from our codebase entirely.

**Daniel**: are there any public notebooks related to calibrating transmon qubits on both qblox and zhinst hardware? OQS has them but they are proprietary. Daniel and Kelvin will discuss in private. In quantify-scheduler!465 Ksenia has implemented a few examples related to T1 and T2 experiments which might be interesting for Daniel as well. 

**Robert**: the public API task listed below is too big. Quantify-scheduler consists of almost 100 modules and 600 class methods and is too big to create a nice overview of modules/classes that should be public/private. We will follow quantify-scheduler#313 and make internals of compilation private and leave the rest alone. We continue as before, whenever new code is added to the code base the author will decide whether this should be public or private. 

**Daniel**: in the near future we would like to have two longer discussions during the developer's meeting. One is about formalizing how hardware parameters can be updated directly inside schedules. The other is about how to introduce and implement subschedules. One of the two topics will probably be presented on the 17th of November. 

### Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

### Public API (No follow up yet since 2022-08-4) [Owner: @rsokolewicz (guidelines, listing)]
Also see https://quantify-hq.slack.com/archives/C02DE4ZENNQ/p1659630403947179?thread_ts=1659541477.414369&cid=C02DE4ZENNQ

We need to discuss about plans for making formal the public APIs so that we do not expose everything within Quantify
as a public API. This is so that any internal implementations would not break a dependent package. (quantify-scheduler#313)
- What is public (and thus breaking, in need of using deprecation scheme upon change), what is private
- Make whole modules private?

**Decision**:
- Whatever user needs is public.
- Guideline: "private, unless..."
- Process: minor changes incorporated to actual work.

### Line lengths (No follow up yet since 2022-08-4) [Owner: @damazdejong]
Requested by Damaz - A proposal to improve the way we work with Quantify styles.

Damaz: Codacy is complaining when Black fails to reformat a string to 88 chars.
Zhihao: Docstrings are not formatted at all.
**Decision**: Move the discussion online.

### Intermediate representation (No follow up yet since 2022-08-4) [Owner: @gtaifu]
Xiang: we are interested in discussion on intermediate representation, especially in the context of Quingo compiler
(https://dl.acm.org/doi/10.1145/3483528).
**Decision**: Xiang will make slides about Quingo and share them in Slack, later we will schedule a slot for presentation.

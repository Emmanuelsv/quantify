## Highlights

### Breaking changes

- (quantify-core!371) Refactored and moved some helper functions from quantify-core to quantify-scheduler.

## Quantify-scheduler

### Merged MRs (highlights)
- Fix UnicodeDecodeError when pip installing from source on Windows (also see quantify-core!367) (quantify-scheduler!464)
- `search_settable_param` fix for finding wrong parameters (quantify-scheduler!461)
- Add xxhash to requirements (quantify-scheduler!460)
- Update checkboxes in MR template (quantify-scheduler!462)

## Quantify-core

### Merged MRs (highlights)
- Fixes for the SI unit conversion (quantify-core!373)
- Update checkboxes in MR template (quantify-core!374)

## Requested discussions / Any other business

### Do we need a SpecPulse operation?
Fokko introduces a discussion point to be discussed this week. There are plans to include a spec pulse operation to Quantify, but he feels treating it as an operation is redundant and should be treated as a pulse instead.  
**Intended outcome**: introduce the topic

```
Quantum circuit layer == device agnostic **abstract** operations?

not all gates have unitary matrix attached
often you don't know it
also, these gates are not fully reversible in practice

So, let's interpret Quantum circuit layer in more general way

Generalized version of spectroscopy pulse is hardware agnostic for different qubit types

gate_library seems to point to quantum information perspective
New library makes it more clear

Follow-ups: create issue and next week conclusion on where to put and whether we should do it at all

Three open questions
- Why do we need this?
- Does this violate our paradigm (circuit/device)
- What are proposed implementations, favorite one?
```

### Edgar: Do we need to release a patch version for core (or: we probably need .. etc)? 
**Intro**: With quantify-core!373 merged and quantify-core!375 merged soon, we have fixed the quantify-core#327 Instrument Monitor init issue occuring for (Qblox) hardware with None values in instrument params  
**Intended outcome**: decision on patch release

**Decision**: 
- critical bug => always hotfix/patch release => so, patch release for core
- Adriaan: please share the follow-up ticket that was created, it may no longer be relevant, given that the fix was different than approaches being suggested before
  - https://gitlab.com/quantify-os/quantify-core/-/issues/329


### Extra discussion

Xiang: 
- Plz look at **Develop a backend for the instrument of EZQ** https://gitlab.com/quantify-os/quantify-scheduler/-/issues/327

Zhihao:
- refactor of zhinst almost done
- How can I help reviewing
  - Will post presented slide deck (on 2022-08-25) in the issue
- Adriaan: 
  - explain ideas behind the refactor: explain design goal (Post slide deck in the issue)
  - can we break up the merge request into smaller MRs
    - per MR explicitly state what changed
  - flag anything controversial, breaking
  - hardware testing: sched contains modules for hardware testing (quantify_scheduler/schedules/verification.py)
    - include a notebook with a description with the hardware setup

Xiang:
- where to find info on connecting qiskit to quantify
- simply interested in the design, how are we going to connect to it
- can we define simple interface for connecting quantify to other software
- Kelvin/Ksenia: still in prototype stage
  - follow-up next week
  - suggest to have an MR (next week) and then can chime in on that (simple t1 experiment): https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/465

## Pending topics (for next DMs)

### External MR (No follow up yet since 2022-08-11)
- Looks like Kelvin needs to review the MR on the zhinst backend. (quantify-scheduler!399)

### Public API (No follow up yet since 2022-08-4) [Owner: @rsokolewicz (guidelines, listing)]
Also see https://quantify-hq.slack.com/archives/C02DE4ZENNQ/p1659630403947179?thread_ts=1659541477.414369&cid=C02DE4ZENNQ

We need to discuss about plans for making formal the public APIs so that we do not expose everything within Quantify as a public API. This is so that any internal implementations would not break a dependent package. (quantify-scheduler#313)
- What is public (and thus breaking, in need of using deprecation scheme upon change), what is private
- Make whole modules private?

**Decision**:
- Whatever user needs is public.
- Guideline: "private, unless..."
- Process: minor changes incorporated to actual work.

### Line lengths (No follow up yet since 2022-08-4) [Owner: @damazdejong]
Requested by Damaz - A proposal to improve the way we work with Quantify styles.

Damaz: Codacy is complaining when Black fails to reformat a string to 88 chars.
Zhihao: Docstrings are not formatted at all.
**Decision**: Move the discussion online.

### Intermediate representation (No follow up yet since 2022-08-4) [Owner: @gtaifu]
Xiang: we are interested in discussion on intermediate representation, especially in the context of Quingo compiler (https://dl.acm.org/doi/10.1145/3483528).
**Decision**: Xiang will make slides about Quingo and share them in Slack, later we will schedule a slot for presentation.


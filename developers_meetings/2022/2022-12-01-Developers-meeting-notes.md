## Epics (APSMM23 release)

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize hardware configuration** (spearheaded by: Edgar)
  - https://gitlab.com/quantify-os/quantify-scheduler/-/issues/332 Update CompilationConfig with high-level structure
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Daniel)
- **Parallel work**:
  1. [milestone 3](https://gitlab.com/groups/quantify-os/-/milestones/3)  Quantify Platform release aka The Website :tm: (remaining: website + RTD replacement)
  1. Remove deprecated code / usage
  1. Clean up open tickets (**247** open and counting [board](https://gitlab.com/groups/quantify-os/-/boards))
  1. Fast feedback in Quantify (first MRs) (Qblox)
  1. https://gitlab.com/quantify-os/quantify-scheduler/-/issues/320 ProfiledScheduleGettable needs refactoring (Qblox)
  1. https://gitlab.com/quantify-os/quantify-scheduler/-/compare/main...QAE-472-Draft-parallel-hardware-prepare-support (Qblox)
  1. ...
 
## Highlights

### Breaking changes

## Quantify-scheduler

### Merged MRs (highlights)
- update tests to use `mock_setup_basic_transmon_with_standard_params` where needed. (quantify-scheduler#369, quantify-scheduler!522)
- Refactor tests of `optical_measurement` function. (quantify-scheduler!523)

## Quantify-core

### Merged MRs (highlights)

## Requested discussions / Any other business

### Cluster naming in hardware config
- @konstantin-orangeqs wants to gather initial opinions and make us aware. (quantify-scheduler#375)

```
We will attach to https://gitlab.com/groups/quantify-os/-/epics/1 and make sure it is taken into account in the new design for the hardware config 
Also Konstantin will open MR to adjust the docsting, Edgar will review n merge
```

### Adding generic_instrument component with string
- @fdvries wants to know why we want to allow init of the generic_icc with a string instead of `InstrumentBase` of QCoDeS. (quantify-scheduler#365)

```
Kelvin: we need the string option for the InstrumentCoordinator
Slava: will chip in a nice way to allow for choosing between string and Instrument as input to the constructor
```

## Extra discussion

### Request for 10-15 minute demo next week
- @kel85uk would like to reserve ~15~ 30 mins for a technical demo on the new qiskit-quantify interface. (quantify-scheduler!465). Want to discuss with other developers on who can and have the motivation to pick it up in terms of making it mature.

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)  

### Formalizing how hardware parameters are updated inside schedules [organized by @djweigand and/or @damazdejong]





## Highlights

### Breaking changes

## Quantify-scheduler

### Merged MRs (highlights)


## Quantify-core

### Merged MRs (highlights)
- Restore initial_value of InstrumentMonitor update_interval param (quantify-core!375)

## Requested discussions / Any other business

### Fokko: (Very) Quick follow-up to last weeks SpecPulse intro
- https://gitlab.com/quantify-os/quantify-scheduler/-/issues/330
- Intended outcome: Just a shoutout to look at the issue and contribute to the online discussion. Stay tuned for conclusion next week!

### Adriaan: Requested discussion, what should the Hardware Config be?
- Required reading: https://gitlab.com/quantify-os/quantify-scheduler/-/issues/222#note_1091786144.
- Intended outcome: is that we share some historical context and identify what needs to be done to converge (could be what questions need to be addressed or identifying what is controversial).
- Follow-up: there will be a follow-up whiteboard session next week Tuesday

**Discussion notes**

- controversies / unconverged on:
  - where to put the mutuable elements, device config?
  - explicit split
    - hardware corrections (control hardware output layer)
    - hardware map (control hardware instruction layer)

  - Fokko worry: need to generalize shouldn't hinder performance of underlying control hardware
  - static vs non-static params: corrections not a good name, may contain various other non-static params

- Related: comment on https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/458
  - Adriaan: qcodes param mirroring: already have the qcodes instrument params
    - mirrors the exisiting params in the hardware,
    - adds some logic to help the user (e.g. translate for complex_output I Q to the qblox qcodes params),
    - but also makes it less clear, e.g. what are the allowed values
  - optional params in hardware config error prone, as when present, they override what might get set dynamically

- @slavoutich notes post meeting: This is [my braindump](https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/403)
  about a declarative definition of a setup and a [RTD documentation for this MR](https://quantify-quantify-scheduler--403.com.readthedocs.build/en/403/planned/redesign.py.html).
  During the developer's meeting I talked about idea of channels, but here this is exactly where I stopped.
  As usual, I may forget what I was thinking when I was writing that, but basically,
  high level user experience I want to achieve, is:

  ```python
  from quantify_scheduler import QuantifySetup
  from quantif_scheduler.devices import UHFQA, HDAWG8, SPIRack, RhodeShwarzBlah, AnapicoBlah
  from quantif_scheduler.channels import IQMixerChannel, DirectConnection

  from proprietary_user_codes import QuantumDeviceDesign, AnotherQuantumDeviceDesign

  class Setup(metaclass=QuantifySetup):  # That may be needed to enable magic later
      # We may characterize several devices simultaneiously
      device1 = QuantumDeviceDesign()
      device2 = AnotherQuantumDeviceDesign()

      # Define all the hardware we have
      uhfqa1 = UHFQA(address="192.168.10.1")
      uhfqa2 = UHFQA(address="192.168.10.2")
      hdawg1 = HDAWG8(address="192.168.10.3", channel_mode=2)
      spi = SPIRack(address="192.168.10.4")
      lo1 = RhodeShwarzBlah()
      lo2 = AnapicoBlah()

      q0_ro_ouptput = IQMixerChannel(lo1.ch1, uhfqa1.input1, uhfqa1.input2, device1.q0.readout_port)
      q0_flux_dc = DirectConnection(spi.ch1, device1.q0.flux_port)
      q0_flux_ac = DirectConnection(hdawg1.ch1, device1.q0.flux_port)

      ...

  setup = Setup(device1="Device1_rev10", device2="Device2_rev42")  # Perhaps we want to enable persistent storage of parameters per device ID

  meas_ctrl = setup.connect()
  meas_ctrl.settables(...)
  meas_ctrl.setpoints(...)
  meas_ctrl.gettables(...)
  meas_ctrl.run()
  ```

### Adam: Hardware debugging in quantify
- Introduce: https://gitlab.com/quantify-os/quantify-core/-/issues/328
- Intended outcome: identify possibilities for making the debugging of hardware signals easier in quantify
  - In particular, I would like to make it easier to get a trace of a RF signal, for any given schedule that you can run
- Follow-ups (Adam): populate issue with user story and possible solutions

**Discussion notes**

Run my code exactly as it is, plug my output into something, and get my signal out, specifically using a scope
Not possible in quantify to run original schedule
=> solution: set some mode debug mode, tells QRM to do a trace acquisition

Adriaan: For Qblox hardware, can we output RF on baseband? No
Fokko: what do you want to see?
  - signal => pico scope
  - envelopes => ...

Put it on a scope, without changing anything

Debug mode: start from user story: User wants to see the envelopes of RF signal without changing anything in the schedule

Fokko: created small program that uses QRM as oscilliscope

Adam: Would it work with my rabi schedule

Fokko: yes, but with some manual step
  - Plz do note: there already always is some manual step included (for instance swap cable)
  - So, in this approach, a manual step should also be allowed
  - Should be fine, as long as it is not an error prone step (or a step that potentially hides an error)

### Xiang/Shun: Develop a backend for the instrument of EZQ
- Introduce: https://gitlab.com/quantify-os/quantify-scheduler/-/issues/327
- Intended outcome: The main goal is to have a discussion on the plan Shun has made

**Discussion notes**

Shun: First thing: write test
  - where to put
    - hardware_cfg added to mock_setup.py
    - Add test_ezq_backend.py to graph_backends

Adriaan: Adding a test notebook that shows example of how the backend should run and what expected outcome should be
  - Preferably part of documentation
  - Alternatively, adding it to the MR description
  - We can only merge this if it is running on hardware

Create a fork
  - MR from the fork
  - Kelvin will create mirror MR so pipeline works

### Kelvin/Kesnia: Follow up discussions on Qiskit-Quantify available
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/465

**Discussion notes**

- Kelvin: Brief update: we have an interface prototype
- Please give us one week to address questions by Xiang

- Should be able to run t1 relatively easily using the quantify backend (this is how qiskit sees us)
- Does the qiskit interface provide recommendations for how to handle outcome of expirements: no, so we use xarray

### Extra discussion

Zhihao: willing to pick up formalizing the hardware config for the Zhinst backend
- Do refactor of zhinst backend first
- Then can turn to formalizing the hardware config


Edgar:
- `mock_setup` will get replaced by `mock_setup_basic_transmon` (see https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/457)


## Pending topics (for next DMs)

### External MR (No follow up yet since 2022-08-11)
- Looks like Kelvin needs to review the MR on the zhinst backend. (quantify-scheduler!399)
- Related to the discussion raise since 2022-09-01
```
Zhihao:
- refactor of zhinst almost done
- How can I help reviewing
  - Will post presented slide deck (on 2022-08-25) in the issue
- Adriaan:
  - explain ideas behind the refactor: explain design goal (Post slide deck in the issue)
  - can we break up the merge request into smaller MRs
    - per MR explicitly state what changed
  - flag anything controversial, breaking
  - hardware testing: sched contains modules for hardware testing (quantify_scheduler/schedules/verification.py)
    - include a notebook with a description with the hardware setup
```

### Public API (No follow up yet since 2022-08-4) [Owner: @rsokolewicz (guidelines, listing)]
Also see https://quantify-hq.slack.com/archives/C02DE4ZENNQ/p1659630403947179?thread_ts=1659541477.414369&cid=C02DE4ZENNQ

We need to discuss about plans for making formal the public APIs so that we do not expose everything within Quantify as a public API. This is so that any internal implementations would not break a dependent package. (quantify-scheduler#313)
- What is public (and thus breaking, in need of using deprecation scheme upon change), what is private
- Make whole modules private?

**Decision**:
- Whatever user needs is public.
- Guideline: "private, unless..."
- Process: minor changes incorporated to actual work.

### Line lengths (No follow up yet since 2022-08-4) [Owner: @damazdejong]
Requested by Damaz - A proposal to improve the way we work with Quantify styles.

Damaz: Codacy is complaining when Black fails to reformat a string to 88 chars.
Zhihao: Docstrings are not formatted at all.
**Decision**: Move the discussion online.

### Intermediate representation (No follow up yet since 2022-08-4) [Owner: @gtaifu]
Xiang: we are interested in discussion on intermediate representation, especially in the context of Quingo compiler (https://dl.acm.org/doi/10.1145/3483528).
**Decision**: Xiang will make slides about Quingo and share them in Slack, later we will schedule a slot for presentation.

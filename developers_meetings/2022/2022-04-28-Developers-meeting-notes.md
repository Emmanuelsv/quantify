
## Highlights
_None_
  

## Breaking changes
- Potentially a breaking change. Using the `to_gridded` function from `quantify_core.data.handling` now will reset the `grid_2d` attribute back to `False` (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/277).


## Quantify-scheduler

_None_
  

#### Merged MRs (highlights)
_None_
   

## Quantify-core
_None_
  

#### Merged MRs (highlights)
_None_
  

## Requested discussions / Any other business
- We're participating again in the unitary hack (https://unitaryhack.dev/) this year!


## Pending topics (for next DMs)

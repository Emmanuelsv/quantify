## Epics (APSMM23 release)

- https://gitlab.com/groups/quantify-os/-/epics/5 Data handling and acquisition flow
- https://gitlab.com/groups/quantify-os/-/epics/2 Formalize hardware configuration
- https://gitlab.com/groups/quantify-os/-/epics/1 Documentation, User Guide and add New Tutorials

## Highlights

### Breaking changes

## Quantify-scheduler

### Merged MRs (highlights)
- Related to NV centers (quantify-scheduler!507, quantify-scheduler!506, quantify-scheduler!503, quantify-scheduler!504, quantify-scheduler!490, quantify-scheduler!493)

## Quantify-core

### Merged MRs (highlights)

## Requested discussions / Any other business

### @kel85uk: Suggestion to have a VNA gettable within Quantify
The group of Christian Dickel has a prototype gettable. Would be good to have it in quantify-scheduler.
Goal: Do we want this feature? Can we afford this?

Discussion:
- Adriaan: We're working on StructuredGettable: returns xarray dataset, allowing to return combination of dependent (measured) and indep variables
- Chris: Shouldn't wait with VNA gettable, where does it go, into core, show case / user contrib repo  
- Chris will share module on slack, to facilitate discussion on where it should go

### @kel85uk: Marketing/Branding/Documentation
Christian Dickel also suggested we do regularly a livestreaming of tutorials/documentation of quantify concepts similar to qcodes youtube. He's willing to also contribute if we have a regular group to do the same.
Goal: Do we want this? Can we afford this?

Discussion:
- There's agreement on the added value and that this would be nice to have, but there's thinking required for the angle in which this could be framed to possibly allow people to do this under work hours. Adriaan is going to give it some thought

### Daniel: Qblox / Zhinst schedule compatibility

Given https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/509, users will want to use this asap and also will want examples. The spectroscopy schedules file seems like the natural place for that https://gitlab.com/quantify-os/quantify-scheduler/-/blob/main/quantify_scheduler/schedules/spectroscopy_schedules.py
However, the changes do not work with zhinst backend. Their new hardware can support frequency sweeps, their old hardware cannot.
So do we:
- add new (mostly duplicate) functions to the file
- add flags to the existing sweeps for slow/fast  

If so, what is the default?

Discussion:
- Adriaan: actually different schedule / use case, make it NCO hetrodyne spectr sched
- Add to docstring of old heterodyne_spec_sched, faster way of doing it can be found here


### Reflective meeting - 2022-11-10 [organized by @rsokolewicz]

Robert will host a meeting next week where we can reflect on how we develop quantify. Anything can be discussed: the role of maintainers/developers, how merge requests and issues are handled, about the use of the developer's slack channel, about the developer's meeting... It's really a moment for reflection and to see if there are some frustrations or emotions that need to be addressed and to see if we can improve our workflow/politics/

MIRO board: https://miro.com/app/board/uXjVPFQm28E=/

## Extra discussion

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

### Formalizing how hardware parameters are updated inside schedules [organized by @dweigand and/or @ddejong]

### Implementing sub-schedules [organized by @dweigand and/or @ddejong]

### Public API (No follow up yet since 2022-08-4) [Owner: @rsokolewicz (guidelines, listing)]
Also see https://quantify-hq.slack.com/archives/C02DE4ZENNQ/p1659630403947179?thread_ts=1659541477.414369&cid=C02DE4ZENNQ

We need to discuss about plans for making formal the public APIs so that we do not expose everything within Quantify
as a public API. This is so that any internal implementations would not break a dependent package. (quantify-scheduler#313)
- What is public (and thus breaking, in need of using deprecation scheme upon change), what is private
- Make whole modules private?

**Decision**:
- Whatever user needs is public.
- Guideline: "private, unless..."
- Process: minor changes incorporated to actual work.

### Line lengths (No follow up yet since 2022-08-4) [Owner: @damazdejong]
Requested by Damaz - A proposal to improve the way we work with Quantify styles.

Damaz: Codacy is complaining when Black fails to reformat a string to 88 chars.
Zhihao: Docstrings are not formatted at all.
**Decision**: Move the discussion online.

### Intermediate representation (No follow up yet since 2022-08-4) [Owner: @gtaifu]
Xiang: we are interested in discussion on intermediate representation, especially in the context of Quingo compiler
(https://dl.acm.org/doi/10.1145/3483528).
**Decision**: Xiang will make slides about Quingo and share them in Slack, later we will schedule a slot for presentation.


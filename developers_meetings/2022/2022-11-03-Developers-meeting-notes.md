## Highlights

### Breaking changes

## Quantify-scheduler

### Merged MRs (highlights)

- qblox - add mixing field and update assign frequency function (quantify-scheduler!482)
- Add Deprecation Warning for `acq_channel` Keyword in Measure (Hotfix) (quantify-scheduler!491)
- git - change git merge strategy to union for changelog.md and authors.md to prevent recurrent merge conflicts (quantify-scheduler!495)

## Quantify-core

### Merged MRs (highlights)

- pytest - Disable insmon and plotmon windows popping up during Pytest runs (quantify-core!400)
- Support qcodes >0.34 by changing requirements.txt (quantify-core!382)
- git - change git merge strategy to union for changelog.md and authors.md to prevent recurrent merge conflicts (quantify-core!399)
- Update docstring of multi_experiment_data_extractor (quantify-core!398)

## Requested discussions / Any other business

### @fdevries: Ports and signal directions

> I would like to discuss Ports and signal directions (in/out) in the upcoming dev meeting. Our ports do not have information on it being an input or output now, therefore we chose to use two different ports in the NV_element.py for 'control' and 'readout'. This choice might be paradigm breaking, that is why I think we should discuss it.

- Ports are strings for now, and we are satisfied with that. In the future we may add there port capabilities and validate them.
- Everyone on a meeting agreed that operations should have input and output ports by default, and we will take that into account while development in future. (Before we were thinking that input and output port is the same.)

## Extra discussion

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

### Reflective meeting - 2022-11-10 [organized by @rsokolewicz]

Robert will host a meeting next week where we can reflect on how we develop quantify. Anything can be discussed: the role of maintainers/developers, how merge requests and issues are handled, about the use of the developer's slack channel, about the developer's meeting... It's really a moment for reflection and to see if there are some frustrations or emotions that need to be addressed and to see if we can improve our workflow/politics/

More info to come.

### Formalizing how hardware parameters are updated inside schedules [organized by @dweigand and/or @ddejong]

### Implementing sub-schedules [organized by @dweigand and/or @ddejong]

### Public API (No follow up yet since 2022-08-4) [Owner: @rsokolewicz (guidelines, listing)]
Also see https://quantify-hq.slack.com/archives/C02DE4ZENNQ/p1659630403947179?thread_ts=1659541477.414369&cid=C02DE4ZENNQ

We need to discuss about plans for making formal the public APIs so that we do not expose everything within Quantify
as a public API. This is so that any internal implementations would not break a dependent package. (quantify-scheduler#313)
- What is public (and thus breaking, in need of using deprecation scheme upon change), what is private
- Make whole modules private?

**Decision**:
- Whatever user needs is public.
- Guideline: "private, unless..."
- Process: minor changes incorporated to actual work.

### Line lengths (No follow up yet since 2022-08-4) [Owner: @damazdejong]
Requested by Damaz - A proposal to improve the way we work with Quantify styles.

Damaz: Codacy is complaining when Black fails to reformat a string to 88 chars.
Zhihao: Docstrings are not formatted at all.
**Decision**: Move the discussion online.

### Intermediate representation (No follow up yet since 2022-08-4) [Owner: @gtaifu]
Xiang: we are interested in discussion on intermediate representation, especially in the context of Quingo compiler
(https://dl.acm.org/doi/10.1145/3483528).
**Decision**: Xiang will make slides about Quingo and share them in Slack, later we will schedule a slot for presentation.

## Highlights

### Breaking changes

- Rarely used functionality was removed from `quantify-core` (quantify-core!378, quantify-core!379)
- If `analysis = Analysis(tuid).run()` fails, instead of raising and exception it will return a partially complete
  analysis. Exceptions are intercepted and logged instead. (quantify-core!379).

## Quantify-scheduler

### Merged MRs (highlights)

- Qblox backend: it is now possible to set input output gain attenuation from quantify (quantify-scheduler!458)

## Quantify-core

### Merged MRs (highlights)

*Empty*

## Requested discussions / Any other business

### Question from Robert

I have an MR that is almost ready be merged.
I made some changes that should improve some of our qblox-users' experience by improving the Measurement operation
for qblox devices, while zhinst-users should remain unaffected. We feel strongly to have the code tested on both qblox
and zhinst hardware before adding it to a new quantify-scheduler-release.
That being said, testing on zhinst hardware will have to wait for a few weeks and at the same time we (qblox devs) feel
strongly to have this merged rather quickly. Do people here have an opinion about if we go about this way: merge the MR
into main, test on zhinst hardware in a few weeks, if all goes well add it to the next quantify-scheduler-release.
If something goes wrong along the way we patch/fix/revert.
edit: it is basically a question if it is acceptable to have code in the main branch that should ideally be tested on
hardware but isn't.

MR quantify-scheduler!434

Outcome: we merge to `main`, and before creating a sched release we make sure this has been tested on zhinst hardware
- Robert will make sure the zhinst test is not forgotten


### Conclusion to the discussion on SpecPulse
- https://gitlab.com/quantify-os/quantify-scheduler/-/issues/330
- Intended outcome: conclusion on whether we need it and where to place the code

Konstantin (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/330#note_1101844539):
1. rename to SpectroscopyOperation
2. nv_library (leaving it in the circuit library): one lib for each qubit arch
3. leave gate_library unchanged

Adam: uses it too for transmon, so nv_libary not best place, new spectroscopy lib perhaps

Slava: is not so important for normal user, as they interact with DeviceElement

On review:
Slava: plz insert reverse compatibilty aliases where required (e.g. `@deprecated`)

Outcome: **Qblox SE will review before next dev meeting** (same promise as last week marking it bold now)

### Third-party backends (@slavoutich)

- There was an internal discussion by maintainers about how we manage third-party backends.
- *Intended outcome*: people get informed and can ask questions or give suggestions.

What is a backend: turns a sched into a compiled sched (instructions for control hardware)

To keep things manageable/reviewable for the maintainers:

- Third-party (i.e., not managed by Quantify Consortium) backends should go into separate packages, instead of the quantify scheduler package

- To be able to do so, we need a clear interface

- We will link third party backend packages on the Quantify website being created

- Also, the developer meeting / Slack chat is still the place to discuss or ask for help

```
Zhihao: we need a clear interface
between sched and backend, currently it is timing table
formally defined intermediary representation
Xiang: internal representation, binary rep of the experiment
Instruction based interface for quantify

Currently timing table intermediary rep => into instruction based 
Cannot parallelize it
For e.g. 36-qubit system, scalability becomes very important

Christian: also problem with qcodes
everythng meant to run in single process
big problem to solve

Xiang: want to emph importance of the scalablitlity, need to parallelize

Christian: QTlab, predecessor of qcodes, actually had master slave notion, allowing for parallelization
datastructure needs to be parallelizeable
```

Outcome: we will need to put this on the agenda again (some people are on holiday or conferences now):
- What are the requirements for the interface for the backend
- What intermediary representation do we need, how is it different from current `quantify_scheduler.Schedule`
- We best organize a separate meeting
- Also see Xiang's listed follow-up point on IR



### Extra discussion

*Empty*

## Pending topics (for next DMs)

### Public API (No follow up yet since 2022-08-4) [Owner: @rsokolewicz (guidelines, listing)]
Also see https://quantify-hq.slack.com/archives/C02DE4ZENNQ/p1659630403947179?thread_ts=1659541477.414369&cid=C02DE4ZENNQ

We need to discuss about plans for making formal the public APIs so that we do not expose everything within Quantify
as a public API. This is so that any internal implementations would not break a dependent package. (quantify-scheduler#313)
- What is public (and thus breaking, in need of using deprecation scheme upon change), what is private
- Make whole modules private?

**Decision**:
- Whatever user needs is public.
- Guideline: "private, unless..."
- Process: minor changes incorporated to actual work.

### Line lengths (No follow up yet since 2022-08-4) [Owner: @damazdejong]
Requested by Damaz - A proposal to improve the way we work with Quantify styles.

Damaz: Codacy is complaining when Black fails to reformat a string to 88 chars.
Zhihao: Docstrings are not formatted at all.
**Decision**: Move the discussion online.

### Intermediate representation (No follow up yet since 2022-08-4) [Owner: @gtaifu]
Xiang: we are interested in discussion on intermediate representation, especially in the context of Quingo compiler
(https://dl.acm.org/doi/10.1145/3483528).
**Decision**: Xiang will make slides about Quingo and share them in Slack, later we will schedule a slot for presentation.

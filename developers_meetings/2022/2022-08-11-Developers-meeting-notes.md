## Highlights
- Quantify-scheduler version 0.8.0 is now released! (https://pypi.org/project/quantify-scheduler/)
- Quantify-core version 0.6.3 is now released! (https://pypi.org/project/quantify-core/)

## Breaking changes
- There are breaking changes, so please have a look at the respective packages' changelogs.

## Quantify-scheduler
- Some temporary requirements have been removed. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/447)

#### Merged MRs (highlights)
_None_

## Quantify-core
- Some temporary requirements have been removed. (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/368)

#### Merged MRs (highlights)
_None_

## Requested discussions / Any other business

### New format of the DM
Maintainers: We feel the meeting has to be snappier/more to the point. Please post any agenda points you want to raise in the respective slack #software-for-developers channel (https://quantify-hq.slack.com/archives/C02DE4ZENNQ) prior to the meeting instead of private messages to the maintainers. Have a desired outcome of the discussion point, and add any extra relevant information for people to understand/contribute to effectively in the meeting. In the end, you will be the owner of the discussion/task requested and if not, please raise who you would like to follow up/raise it in the meeting for us to find the group/developer to follow up on.

We agree that discussions should aim to reach a conclusion after about 5 minutes. If that doesn't happen, move the discussion outside the developers meeting. 

### Implementation question
Fokko de Vries:
```
I would like to discuss the following use-case, and how to properly implement it in quantify-scheduler:
Qubit transitions driven by lasers have clock frequencies of order 500 THz.
Typically these lasers are turned and off by optical modulators driven by electronic signals.
The optical modulator works at certain frequencies, i.e. 100 MHz, not related to the 500 THz.
Turning the laser light on/off is governed by sending a 100 MHz square pulse, where the amplitude scales the laser power.
Q: what is the proper way to implement this in quantify-scheduler?
(What frequency shall we store in a clock? Where to include the optical modulator frequency? (pulse, pulse factory, clock). How to ensure the numerical oscillator is used instead of the waveform memory for all backends?)
```

possible solutions:
- Treat it as a double upconversion with a second mixer. In this case a list of two frequencies to the LO
- In the compilation an extra layer can be included with modulation frequencies that generates the correct pulse
- Into the hardware config if you treat the new frequency as the LO frequency
- Upconverting the signal twice, but "forget" each intermediate frequencies

### Extra discussion
Zhihao is waiting for review on his MR (!410). Kelvin agrees to take a look if Christian Dickel doesn't do so in the next few days. In this MR's description it states that this resolves issue #295, but it doesn't, so this statement will be removed.

One of OQS' interns, Ksenia, is going to work on combining Qiskit with Quantify. Zhihao tried this before, but didn't get satisfactory results. Xiang is also interested in this. Let's all work together. 

Fokko introduces a discussion point to be discussed next week. There are plans to include a spec pulse operation to Quantify, but he feels treating it as an operation is redundant and should be treated as a pulse instead. Think about it, we will discuss next week. 

## Pending topics (for next DMs)

### Public API (No follow up yet since 2022-08-4)
We need to discuss about plans for making formal the public APIs so that we do not expose everything within Quantify as a public API. This is so that any internal implementations would not break a dependent package. (quantify-scheduler#313)
- What is public (and thus breaking, in need of using deprecation scheme upon change), what is private
- Make whole modules private?

**Decision**: 
- Whatever user needs is public.
- Guideline: "private, unless..."
- Process: minor changes incorporated to actual work.

Include new checkboxes on MR?
- Changed functionality: public API so should we deprecate? => checkbox: n/a, done
- New functionality: should it be private? => checkbox: n/a, done
- Should be tested on hardware? => checkbox: n/a, done

**Decision**:
- Everyone is welkome to improve template by sending an MR, but probably this will be done by maintainers.
- "Manual test" checkbox is needed.

### Documentation (No follow up yet since 2022-08-4)
_Edgar: propose to move this to next week, some of the math statements aren't rendering (https://quantify-quantify-scheduler--349.com.readthedocs.build/en/349/autoapi/quantify_scheduler/operations/gate_library/index.html#quantify_scheduler.operations.gate_library.X90)_
There is an open merge request related to adding documentation to gates (see quantify-scheduler!349).

### Line lengths (No follow up yet since 2022-08-4)
Requested by Damaz - A proposal to improve the way we work with Quantify styles.

Damaz: Codacy is complaining when Black fails to reformat a string to 88 chars.
Zhihao: Docstrings are not formatted at all.
**Decision**: Move the discussion online.

### Intermediate representation (No follow up yet since 2022-08-4)
Xiang: we are interested in discussion on intermediate representation, especially in the context of Quingo compiler (https://dl.acm.org/doi/10.1145/3483528). 
**Decision**: Xiang will make slides about Quingo and share them in Slack, later we will schedule a slot for presentation. 

## Highlights
- Please populate the discussions on the naming convention for qcodes. (https://github.com/QCoDeS/Qcodes/discussions/4289)

## Breaking changes
_None_

## Quantify-scheduler
- MR (quantify-scheduler!399) still to be reviewed.
- Discussions on acquisition indexing available here. (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/36)

#### Merged MRs (highlights)
- Added an extra edge in the example for quantum device. (quantify-scheduler!415)

## Quantify-core
_None_

#### Merged MRs (highlights)
_None_

## Requested discussions / Any other business
- Compilation in [quantify-scheduler !410](https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/410)
  - Christian and Damaz will review MR and give comments
  - user calibration doesn't work properly for Christian
- Adding metadata to dataset in [quantify-core #315](https://gitlab.com/quantify-os/quantify-core/-/issues/315)
  - Agreement that it should be possible to add metadata to dataset
  - Christian uses a dummy instrument and its snapshot functionality to write metadata to file -> works but not nice
  - Slava will take this requirement into account when designing data-saving API
  - Metadata will likely be saved in a separate file
  - Timeline roughly a month
- Fix of secondary plotmon in [quantify-core !349](https://gitlab.com/quantify-os/quantify-core/-/merge_requests/349)
  - will be merged immediately
  - will be included in version 0.6.1 (to be released soon)


## Pending topics (for next DMs)
_None_

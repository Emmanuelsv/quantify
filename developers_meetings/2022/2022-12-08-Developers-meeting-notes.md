## Epics (APSMM23 release)

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize hardware configuration** (spearheaded by: Edgar)
  - https://gitlab.com/quantify-os/quantify-scheduler/-/issues/332 Update CompilationConfig with high-level structure
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Daniel)
- **Parallel work**:
  1. [milestone 3](https://gitlab.com/groups/quantify-os/-/milestones/3)  Quantify Platform release aka The Website :tm: (remaining: website + RTD replacement)
  1. Remove deprecated code / usage
  1. Clean up open tickets (**247** open and counting [board](https://gitlab.com/groups/quantify-os/-/boards))
  1. Fast feedback in Quantify (first MRs) (Qblox)
  1. https://gitlab.com/quantify-os/quantify-scheduler/-/issues/320 ProfiledScheduleGettable needs refactoring (Qblox)
  1. https://gitlab.com/quantify-os/quantify-scheduler/-/compare/main...QAE-472-Draft-parallel-hardware-prepare-support (Qblox)
  1. ...
 
## Highlights

- Resolve "DeprecationWarnings are not visible to end users" (quantify-core!41)
  - In the (near) future we will implement CI failing over FutureWarnings (the replacement way of indicating deprecation visible to end-users by default)
  - https://gitlab.com/quantify-os/quantify-scheduler/-/issues/382


### Breaking changes

## Quantify-scheduler

### Merged MRs (highlights)
- NV CR count quantify-scheduler!502
- Remove deprecated code from production code, tests and documentation in preparation for 0.10.0 release (quantify-scheduler!526)
- Add inputs to qblox backend and refactor assign_frequencies (QAE-601) (quantify-scheduler!497)
- Use tmp_test_data_dir fixture to remove temporary test files (quantify-scheduler!528)
- Describe instrument naming in qblox backend docs (quantify-scheduler!53)

## Quantify-core

### Merged MRs (highlights)
- Remove global parameters from MC unit tests (quantify-core!409)

## Requested discussions / Any other business

### Streamline schedule functions / Schedule v2?
@djweigand 
-  The schedule functions have a very cumbersome interface. 
- The worst offenders are `two_tone_spec` and `heterodyne_spec`, where 11/14 and 8/8 arguments are attributes of `BasicTransmonElement`. And the three exceptions should arguably also be an attribute of `BasicTransmonElement`.
- Alternative solutions (in order of my preference):
  1. schedule functions take a DeviceElement and use duck typing and optional kwargs to fill any missing arguments.
  2. define new schedule functions doing the above, and leave the old functions in place
  3. provide helper functions to translate a DeviceElement to the arguments of the old schedule functions

Discussion:
- Forcing use of device element, is that okay? No
- Optional DeviceElement param is 
- Issue created: https://gitlab.com/quantify-os/quantify-scheduler/-/issues/380

### Streamline compiler arguments 
@djweigand  
- Is there a reason why `QuantifyCompiler.compile()` does not (optionally) take a `QuantumDevice` as second argument? 
- Something like this is quite common and could be streamlined easily (see quantify-scheduler!535) 

Outcome:
- see MR https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/535
- going for the following design: https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/535#note_1207180303


### Negative latencies support in Qblox backend?
Tobias
- I would like to bring up https://gitlab.com/quantify-os/quantify-scheduler/-/issues/379, which is about dealing with negative latencies in the compilation

Discussion:
- Kelvin: needed soon
- Daniel: we could potentially use the implementation as in the zhinst backend
- Tobias: will make sure this gets followed up on, add example to ticket and post in slack

### Qiskit-Quantify Interface
Kesnia / Kelvin
- 30 mins for a technical demo on the new qiskit-quantify interface. (quantify-scheduler!465)
- Want to discuss with other developers on who can and have the motivation to pick it up in terms of making it mature.

```
Idea: allow for Qiskit schedule in ScheduleGettable

We don't have a pulse simulator (Ksenia was using the OQS proprietary one)

Idea: Wrapper around pulse simulator of Qiskit

How about manually run the tests

Mark interface as unstable

Some basic tests, some mocks

Just test if it is compiles

Daniel: Would like to show it at the march meeting, if we can use native qiskit circuits or import them
```

## Extra discussion

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)  

### Formalizing how hardware parameters are updated inside schedules [organized by @djweigand and/or @damazdejong]






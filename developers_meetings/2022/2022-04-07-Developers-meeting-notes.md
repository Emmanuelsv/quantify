## Highlights

- Harold and Pieter are now developers in the quantify-core and quantify-scheduler.

  

## Breaking changes

_None_

## Quantify-scheduler

  

- New compilation backend has unwanted clocks bug for qblox backend. (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/278, https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/371). Fix testing on actual hardware still in progress (_mentioned before in 2022-03-24 meeting notes_)

  

- New qblox-instruments driver: Release early next week containing updated InstrumentCoordinator Qblox components (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/377)


- Firmware, driver, quantify needs to be updated.

  

#### Merged MRs (highlights)

_None_

  
  

## Quantify-core

_None_

  

#### Merged MRs (highlights)

_None_

  
  

## Requested discussions / Any other business

- External MRs have been neglected, and it indicates the bottleneck comes from lack of engagement from the maintainers. Currently only 3 active maintainers (who are also fully loaded). This is not sustainable. Propose to discuss way of working and approval.
	- **Conclusion**: Developers review MRs in detail and approve (fair judgement should be taken to pick a reviewer), maintainers skim code and merge.
	- **Todo (@ereehuis):** add Michel Vielmetter and Christian Dickel to developers

- (by @slavoutich) I think we must adapt https://numpy.org/neps/nep-0029-deprecation_policy.html as a reference to drop Python version support and drop Python 3.7 support (we are still kinda "support" it officially, right?)
	- Discussion about compability of packages. ZI packages support versions up to Python 3.10.
	- **Conclusion**: Drop support for 3.7. Adopt policy as suggested (but don't follow dogmatically if there are good reasons against it).
	- **TODO (Maintainers)**: Mention the adopted policy in the contribution guidelines.

- (by @tobiasbonsen) Another point for discussion: [Commit 1f1f4483 @ quantify-scheduler](https://gitlab.com/quantify-os/quantify-scheduler/-/commit/1f1f448345d2b92ec4ce8664489449732db1e9e9) initialises unknown values in the transmon element to `float("nan")` (instead of `None` ) since this was required by Pydantic ([Issue #274](https://gitlab.com/quantify-os/quantify-scheduler/-/issues/274)). However, it seems that this change leads to multiple issues ([#278](https://gitlab.com/quantify-os/quantify-scheduler/-/issues/278), [#277](https://gitlab.com/quantify-os/quantify-scheduler/-/issues/277), and maybe more in the future). It would be good to discuss if we want to stick to these `float("nan")` values and how it affects the existing software & future work.
	- Issue originates from backends and propagates to instrument coordinator
	- Mainly concerns the case where certain elements are present in the device/hardware config but are not used in the schedule / fully configured 
	- Even though not used, backends try to init the elements, and nan values end up in the instrument coordinator through compilation layer
	- Before, in using `None`, `None` values would be ignored or get replaced by default values ("under the radar")
	- Moving from `None` to `nan` makes that fail now
	- Adriaan: Example: two qubits with parameters, only qubit 1 is calibrated. qubit 2 uncalibrated with nan. 
	- Possible short-term solutions: 
	  - Raise an exception when we encounter `nan` (_e.g. in instrument coordinator, which would then surface these propagated nans so we can track down and fix where they originate?_)
	  - Skip silently?
	  - Example MR where this trade-off comes into play: [https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/378](https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/378)
	- **Conclusion**: Only used elements should be taken into account in the backend compilation layer. Discuss further on quantify-scheduler day


- (by @ereehuis) We need to make sure external MRs get reviewed. Suggestion to assign external MRs that need maintainer attention. Suggest to assign one/two MR per maintainer per week.
	- Discussion: external contributions should have first priority for maintainers. Currently 4 external MRs in quantify-core, 3 in quantify-scheduler. "Review me" label could distinguish between maintainer/developer review. But externals do not have priviliges to est labels on MRs -> need to promote frequent contributors to developers.
	- **Conclusion:** To be discussed further in maintainer's meeting on Tuesday to divide workload between maintainers


## Pending topics (for next DMs)

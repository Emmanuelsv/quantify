## Highlights
Reminder to all:

There will not be a developer meeting this week due to the national holiday of the Netherlands (Liberation day). This serves as this week's update.
  

## Breaking changes
- ZI backend not compatible with 3.10. See (quantify-scheduler!387)


## Quantify-scheduler
- Pipeline currently known to fail due to failure in building of python 3.10 test image.
  

#### Merged MRs (highlights)
- Fixes the zhinst package version. (quantify-scheduler!387)
- Adapt CI pipeline from quantify-os/ci (quantify-scheduler!385)
- Add `acq_protocol` parameter to Measure gate (quantify-scheduler!386)
   

## Quantify-core
- Speed up of the MeasurementControl due to improved data construction and implementation of cache. (quantify-core!333)
  
#### Merged MRs (highlights)
- Nice Insmon Display of bool, Enum, dict (quantify-core!336)
- Fix QHullError in PlotMonitor with two parameters in setpoints (quantify-core!323)
- [Performance] Improve MeasurementControl data construction (quantify-core!333)
- fourier guess for cosine model (quantify-core!335)
- Bump Python version support and rewrite CI (quantify-core!328)
- plot_fit customisation (quantify-core!334)
- [Dependencies] Allow matplotlib>3.5.0 (quantify-core!332)
  

## Requested discussions / Any other business
_None_


## Pending topics (for next DMs)


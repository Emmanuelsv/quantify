## Highlights
- UnitaryHack kick-off on 3rd June (This Friday!!!!). Do remember to sign up to win 100 USD bounties (for non-OrangeQS and non-Qblox members)! (https://unitaryhack.dev/projects/quantify-scheduler/) (https://unitaryhack.dev/projects/quantify-core/)

## Breaking changes
_None_

## Quantify-scheduler
- Support intermediate frequency deduction in zhinst backend (quantify-scheduler!399) is ready for review. Any developers with Zhinst backends, please test on your hardware to see if it works and approve the MR? 

#### Merged MRs (highlights)
- Requirements relaxed for `qblox-instruments>=0.6.0` (quantify-scheduler!401)
   

## Quantify-core
- Removes the lazy evaluation behaviour of the create_figure method of the analysis classes. This is a temporary revert of some of the memory leakage fix introduced in v0.6.0 which broke class inheritance methods. (quantify-core!345, quantify-core#313)
  
#### Merged MRs (highlights)
_None_
  

## Requested discussions / Any other business
_None_


## Pending topics (for next DMs)
_None_



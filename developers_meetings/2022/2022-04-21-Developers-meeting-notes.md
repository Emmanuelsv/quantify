
## Highlights

- Quantify-Scheduler brainstorm day 14-04-2022 (see https://gitlab.com/quantify-os/quantify/-/blob/main/220414_quantify_scheduler_day/summary.md):
  - **Sessions**:
    - `OUR  IDEAL  HARDWARE  BACKEND.`
    - Prep for future introduction of `feedback` / `classical logic`

  - **Conclusions**:
    1. We can **proceed with compiler ideas** from whiteboard session. (see above). 
    1. The **timing table** does **not** need to be fundamentally **changed** to **accommodate classical logic**. 
    1. **Short-term logic** features consists of "binary" logic and **can be prototyped**. 
    1. **Longer-term logic** requires high-level programming functionality and is **out of scope for now** but may involve concepts like _kernels_. 

  - **Follow-ups**:
    1. **Next steps** and **responsible people** still need to be defined: ...
    1. The devil is in the details, we **converged** on some very important **high-level** subjects but **did not go into** the detail where the **limitations for users** show clearly. 
    1. A **session** needs to be planned to discuss the **measurement/acquisition model**.
  

## Breaking changes

_None_


## Quantify-scheduler

_None_
  

#### Merged MRs (highlights)

- New compilation backend has unwanted clocks bug for qblox backend (_mentioned first in 2022-03-24 meeting notes_) (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/278, https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/371)
   

## Quantify-core

_None_
  

#### Merged MRs (highlights)

_None_
  

## Requested discussions / Any other business


- Carry overs from last DM:
  1. Adopting https://numpy.org/neps/nep-0029-deprecation_policy.html (but don't follow dogmatically if there are good reasons against it)
	 - **In progress (@slavoutich)**: https://gitlab.com/quantify-os/quantify-core/-/merge_requests/328
	   - Mention the adopted policy in the contribution guidelines
	   - Adjust pipelines, drop 3.7, include 3.9
  
  2. Reviewing load:
     - Split "Review me!" label to "Review (maintainer)" / "Review me (developer)": **Done**

- @christiandickel8: I have a ZHInst backend point on the limitations on the number of instructions on the UHFQA. 
  - The current way seqc code is written makes minimal use of loops, but
  - in many cases this could save a lot of instruction memory and enable experiments with more points on the x-axis.     


## Pending topics (for next DMs)

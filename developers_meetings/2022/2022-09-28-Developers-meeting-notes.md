## Highlights

### Breaking changes
- Disallow "\_" in DeviceElement names to comply with qcodes versions 0.34 and up. Also enforces "\_" as the separator between device elements in edge names.  (quantify-scheduler#300, quantify-scheduler!473)

## Quantify-scheduler

### Merged MRs (highlights)
- Pydantic-based model is now used to validate latency corrections. (quantify-scheduler!467, quantify-scheduler#333)

## Quantify-core

### Merged MRs (highlights)
- Reduce quantify-core import time (quantify-core!366)

## Requested discussions / Any other business

### Conclusion to the discussion on SpecPulse
- https://gitlab.com/quantify-os/quantify-scheduler/-/issues/330
- Intended outcome:
	- Get approval for spectroscopy pulse implementation below
	- alternatively: find out how to best reach conclusion without further delay
Latest suggestion:
Konstantin (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/330#note_1105029474):
1. rename to SpectroscopyOperation
2. shared_library (leaving operation on circuit level)
3. leave gate_library unchanged

### Extra discussion

*Empty*

## Pending topics (for next DMs)

### Public API (No follow up yet since 2022-08-4) [Owner: @rsokolewicz (guidelines, listing)]
Also see https://quantify-hq.slack.com/archives/C02DE4ZENNQ/p1659630403947179?thread_ts=1659541477.414369&cid=C02DE4ZENNQ

We need to discuss about plans for making formal the public APIs so that we do not expose everything within Quantify
as a public API. This is so that any internal implementations would not break a dependent package. (quantify-scheduler#313)
- What is public (and thus breaking, in need of using deprecation scheme upon change), what is private
- Make whole modules private?

**Decision**:
- Whatever user needs is public.
- Guideline: "private, unless..."
- Process: minor changes incorporated to actual work.

### Line lengths (No follow up yet since 2022-08-4) [Owner: @damazdejong]
Requested by Damaz - A proposal to improve the way we work with Quantify styles.

Damaz: Codacy is complaining when Black fails to reformat a string to 88 chars.
Zhihao: Docstrings are not formatted at all.
**Decision**: Move the discussion online.

### Intermediate representation (No follow up yet since 2022-08-4) [Owner: @gtaifu]
Xiang: we are interested in discussion on intermediate representation, especially in the context of Quingo compiler
(https://dl.acm.org/doi/10.1145/3483528).
**Decision**: Xiang will make slides about Quingo and share them in Slack, later we will schedule a slot for presentation.

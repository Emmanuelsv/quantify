## Highlights
_None_

## Breaking changes
_None_

## Quantify-scheduler
_None_

#### Merged MRs (highlights)
- Fix 90 degree phase shift for real_output_mode in Qblox backend (QAE-173) (quantify-scheduler!412)
- Docs: Clarify port and clock concepts (quantify-scheduler!431)
- Operations - Rxy theta rotations now fall into the domain of -180 to 180 degrees (quantify-scheduler!433)

## Quantify-core
_None_

#### Merged MRs (highlights)
_None_

## Requested discussions / Any other business

- Edgar:
  - Harold opened two issues for core:
    - AttributeError: InstrumentMonitor object and its delegates have no attribute widget (https://gitlab.com/quantify-os/quantify-core/-/issues/319)
    - PlotMonitor: RuntimeError: wrapped C/C++ object of type PlotDataItem has been deleted (https://gitlab.com/quantify-os/quantify-core/-/issues/320)   
  - Should we briefly discuss these?
  - In general, I'd like to clarify what (immediate or otherwise) follow-ups we couple to issues opened by external reporters

  *Discussion:* whether it is needed to open an issue before opening a MR. We have concluded that for minor fixes it is not really
  needed, but frequently it is useful to communicate to others that you are working on it, i.e. if the feedback on design is needed 
  or someone is already working on it. Good way to communicate it is opening draft MR, even without new commits in it.

  *Request from @haroldmeerwaldt*: if someone opens an issue, feedback from maintainers about their plans about it. *Answer from maintaniers*: Yes.

  *Question from @haroldmeerwaldt*: what are the plans for dataset v2. Answer from @slavoutich: going slowly. First step is to provide dataset
  API, offload data structure to settables and gettables, and dataset API will follow, preserving backwards compatibility 
  for legacy gettables and settables. 

## Pending topics (for next DMs)
_None_




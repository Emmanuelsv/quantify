## Present:

### Maintainers and Project Owners

- [ ] Kelvin
- [X] Damien
- [X] Slava
- [X] Adriaan (PO)
- [X] Jules (PO)

### Community

- [ ] Jordy
- [ ] Adam
- [ ] Diogo
- [ ] Gijs
- [ ] Damaz
- [ ] Vraj

## Introduction new member:

- [ ] Tobias
- [ ] Achmed

## Highlights:

Milestones - Comment Adriaan: update milestone deadlines to be more realistic

### Quantify-scheduler:
- Present milestones. (https://gitlab.com/quantify-os/quantify-scheduler/-/milestones/3)


#### Merged MRs (highlights)

-  Deprecated `plot_circuit_diagram_mpl` and `plot_pulse_diagram_mpl` in `ScheduleBase` in favour of `plot_circuit_diagram` and `plot_pulse_diagram`
   - https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/313
-  Bugfixes Qblox backend and ICC:
   - https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/318
   - https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/319
   - https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/323

### Quantify-core:
- Please mind we set the deadline for a 1.0 release before the 14th of March.
  - Milestones: https://gitlab.com/quantify-os/quantify-core/-/milestones/9
- Deprecation policy 
  - Please take a look -> Do we need a deadline for merge? - Will set deadline after Developers meeting Jan 14th
  - https://gitlab.com/quantify-os/quantify-core/-/merge_requests/282

#### Merged MRs (highlights):

- Correct cosine instrument mistake tutorial 1
  - First merge request by Tobias Bonsen
  - https://gitlab.com/quantify-os/quantify-core/-/merge_requests/284
  
- Use not operator when checking fit success
  - https://gitlab.com/quantify-os/quantify-core/-/merge_requests/261


## Requested discussions:

- QuantumDevice setup from device_config (Diogo Valada):
  - https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/315
  - Please everyone take a look at this
  - Adriaan: for transmons it is natural to use current structure with device config and hardware config. For other types of qubits this might be different, because they are not as mature.

## AOB:
- Worth having a release of the scheduler soon?
  - Adriaan: time for a 0.5.x release 
  - Damien agrees and will take the lead om this
- Adriaan: schedulable MR
  - Currently: in schedule "blocks are added to a timeline" but in reality they are added to a dictionary with timing constraints
  - MR is aimed at changing this structure
  - Important that people look at this 

## Pending topics (for next MMs):

- Rename default branch to main
  - https://gitlab.com/quantify-os/quantify/-/issues/12
  - Adriaan: scared about side-effects, but decide to do it
  - Damien: need someone to take the lead, Adriaan: one of the maintainers should do this - decide next week


## Agenda 

* Discuss first full-featured quantify milestone. 
* Discuss bit more on Agile and process. 
* Discuss issue board. 
     * Discuss scheduler next steps/important issues. 
     * Discuss important next features for core. 
* Any other business 

### First release of quantify

- Discussed the first release agreed on key features. 
- We think this is roughly feasible, need to assess in more detail after specifying issues in more detail. 
- Time burden will be discussed next week. 
- Adriaan will update the Milestone in the system. 


### Agile discussions 

We have decided to add the following labels 
    - "needs clarification" 
    - "in progress' 
Adriaan will add priority type as Callum suggested. 

The review process 
- merge request is open 
- another person is asked to review. 
- person will either 
     - leave comments, moving it back to workable or
     - hit the approve button 
- if the approve button is clicked, the MR can be merged by the original author. 

### Issue board discussion 

- The issue board will be prepared for the next meeting. 
- Milestone will be used to guide decisions. 

### Other business 

- Victor: Please update your profile picture on GitLab. (does not have to be a photo. 
- Jordy: has everyone uploaded the hackathon code? 

 
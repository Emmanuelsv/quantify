2 announcements by M.A. Rol

- Tomorrow OQS will interview Thomas from Topic to start on Monday.

There is 1 person with maintainer status of each comp. In addition, owners also have maintainer status. All others will be downgraded to developer access level. For MR you need at least one approval from someone with maintainer status or higher. Approval from developers is optional but useful.

Current owner level is board (Jules and Adriaan), current maintainers will be Callum and Kelvin.

- Adriaan bought a scope.

Next topic: release with new resources refactor needs a few more days of work. 

- quantify-core is waiting for AR reviewing the persistence plotting. We identify two changes

    - [ ] change TUID to a more readable form (plots legends in the Qt window)
    - [ ] tutorial can be cleaned up. (instrument creation cleanup, shorten it)

We aim to release both core and scheduler before 17 December

Callum announces he stops from January first, Kelvin volunteers to be ad interim Scrum Master.

Callum will transfer knowledge about our Digital Ocean setup. Jordy and Kelvin will be informed by Callum.

Adriaan will invoice us for half the costs of the ReadTheDocs.

Callum will teach Jordy about the Pulsar Backend.

Installation instructions -> currently don't work on windows.
De-facto solution right now is that we recommend anaconda. This is a bigger issue we need to properly investigate. 

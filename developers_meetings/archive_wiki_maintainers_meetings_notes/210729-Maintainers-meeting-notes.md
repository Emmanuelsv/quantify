Present: Jordy, Victor, Jules, Adriaan, Kelvin, Diogo, Damien

## Current state (Max 30 mins):

### Quantify-core:

* Pipeline for windows currently failing due to test infrastructure.
* Switched default engine from Netcdf to h5netcdf (supports complex numbers).
 - We don't know if multiprocessing works well with out current xarray backend (ask Wolfgang for more details)
* quantify dataset is being re-designed (quantify-core#187) as a consequence of quantify-scheduler#36
  * xarray seems to support all use cases we want almost out of the box, but:
  * We have not converged yet on the naming conventions (for the dimensions, etc)
  * Not all variables have the save length. We will pad with np.nan.
  * How to deal with when the data has been hardware-averaged? Suggestion: use an attribute for this
* docker images not available for external contributors quantify-core#231
 - Kelvin will investigate the matter when he has bandwidth

### Quantify-scheduler:

* New user guide for experiment flow is up quantify-scheduler!161
* quantify-scheduler!157 numerical pulses. Is it still blocked?
  * We have to find the best solution for two-way json encoding compatible with nested structures. Damien will take up on this.
* [quantify-scheduler!162](https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/162)
* Link between IC and MeasurementControl is missing?
* Next release [Decisions and key priorities]
  * We want to release, but we are postponing the decision, probably for next week, because don't have a clear list of what we need/can forego for this next release
  * Quantify-core is ready to be released
  * Quantify-scheduler is being pressured into being released.
    * Kelvin is going to prepare the changelogs and see if it is viable to release quantify-scheduler

## Requested discussions:

* Make Damien and Slava as maintainers too.
  * Damien becomes a maintainer
  * Regarding Slava, the discussion is postponed till the next meeting, when he will be present
* Open MM to public
  * Delay until September

## AOB (max 5 mins):
* Align holidays and see if we need to cancel MMs.
  * Last 3 weeks of August, no maintainers meeting.
* Open the software-for-developers Slack channel to the public?
  * We'll do it in September, due to possibly proprietary content which has to be screened.

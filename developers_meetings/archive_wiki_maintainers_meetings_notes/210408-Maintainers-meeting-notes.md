Present: Victor, Jules, Adriaan, Thomas, Damien, Adam, Jordy

## Current state (Max 15 mins):

### Quantify

* Headers updated in both repos, automated tests added
* (Raised by Victor) Decide on docs dependencies and docstrings format quantify#10
    - @Thomas, any progress on the `TYPE_CHECKING` vs qcodes/plotly?
       No update, maybe next week.

### Quantify-core:

* `load_settings_onto_instrument` does not work in practical setting quantify-core#166

### Quantify-scheduler:

* Qblox pulsar backend (targeted for 0.3.0)
    - Merge request ready early next week.
    - Adriaan would like to test in the setup.
    - Should be relatively easy to replace old backend.

## Discussions:

### Important discussion (max 15 mins):

* (Proposed by Adriaan) Adoption of milestones ([scheduler](https://gitlab.com/quantify-os/quantify-scheduler/-/milestones), [core](https://gitlab.com/quantify-os/quantify-core/-/milestones)) in gitlab for repos management progress tracking
    - Proposed releases contents and time-lines
        - Owners agree on roadmap of quantify-core and quantify-scheduler.
        - Milestones can still be shifted around a bit, especially for the quantify-scheduler.
    - Process: who/how manages the milestones boards?
        - Will be discussed next week when all maintainers are present.

### Quantify (max 15 mins):

* (Raised by Adriaan) Modules namespace and third-party tools quantify-scheduler#51
    - [from prev. meeting] Adriaan explained the problem, most of us agree with its impact.
    - [from prev. meeting] Solution not clear, BUT it will be a MAJOR breaking change.
        - Still under investigation, for a better solution.

* (Raised by Victor) Do not allow MRs to be merged if the docs raise fixable warnings quantify-core!145 (also quantify-scheduler!73)
    - Proposal changed to automated tests in the CI (RTD can still build with warnings)
    - @Thomas, @Kelvin could you give your feedback on this proposal?
        - Let's keep the quantify-core warning free and add a test to check that.
        - For quantify-scheduler this is more work, but needs to be done as well.

### Quantify-core (max 15 mins):
* Other technical issues raised?
    - Nothing raised

### Quantify-scheduler (max 15 mins):
* Other technical issues raised?
    - Nothing raised

## Any Other Business (5 mins):
* (Raised by Victor) quantify-scheduler lacks code and essential documentation, e.g. difficulty in reviewing quantify-scheduler!64
    - **suggestion** add concrete and specific documentation goals to milestones
    
* Jules pointed out that the documentation is already showing the unreleased the Zurich Instruments
backend.
        - Latest branch will be removed from documentation to prevent confusion.
* Next week Diogo (new employee) and Roel (intern) will start and work on Quantify.
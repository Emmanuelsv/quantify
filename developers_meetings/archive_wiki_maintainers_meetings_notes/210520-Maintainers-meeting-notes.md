Note: 210513 was not held due to public holiday in the Netherlands (unavailability of the members).

Present:

## Current state (Max 30 mins):

* Praises for Damien (completing [quantify-scheduler!81](/quantify-os/quantify-scheduler/-/merge_requests/81)), and Thomas (completing [quantify-scheduler!49](/quantify-os/quantify-scheduler/-/merge_requests/49)) and Jordy, Victor, Adriaan (for their effort to help review as well as those good catches).

### Quantify-core:

* We have released v0.4.0!

Key issues and MRs left:

* Review milestones and priorities for v1.0.0
  * Set up experiments demo repo
  * Converge different version to single repo (Qitt, Quantify lab, orange prototypes etc)

### Quantify-scheduler:

* We can release v0.3.0!
* ControlStack (responsibilities)
  * Assign reponsibilities for different parts of the control stack.
  * Thomas split off qblox part
  * Damien pick up !112
  * In future, do smaller MRs

Key issues and MRs left:

* Review milestones and priorities for v0.4.0
  * Issue [#2](/quantify-os/quantify/-/issues/2) is key
  * Issues related to control flow are key

## Extra-discussions:

* Pain points for getting quantify-core v0.4.0 out?
* Address pace of reviews and merges.
* Editors/IDEs of choice.
* Where to place gettables? [quantify-core#208](/quantify-os/quantify-core/-/issues/208), [quantify-scheduler!109](/quantify-os/quantify-scheduler/-/merge_requests/109)
* How to use test datasets for RTD [quantify-core#205](/quantify-os/quantify-core/-/issues/205)
* Timed/delayed settable (suggested by Pieter Eendebak for spin systems). [quantify-core#137](/quantify-os/quantify-core/-/issues/137)
* Calibration points in experiment schedules. [quantify-scheduler#94](/quantify-os/quantify-scheduler/-/issues/94)
* Discussed renaming of quantify scheduler/core

## AOB (max 5 mins):

* We have already been listed as a project within Unitary Hack. Still no MRs...
  * Business directors should promote this
* Kelvin needs to get reimbursed for the donation to UH. Total is for $250.
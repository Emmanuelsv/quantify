210204 Maintainers meeting notes

Proposed agenda: A review meeting

Current state:
- Thanks to Victor, code style for both (Core and Scheduler) now completely in black. #8 (closed)

  - We do not have a linter anymore since the change

  - We should choose between `pylint` (Jordy has experience), or `mypy` (Thomas recommends `mypy` for typing)

  - Import annotations from __future__
- Common helper function already implemented for ZI backend. Thomas can help elaborate?
  - See merge request 49
  - New package dataclasses json (similar to structs but for python)
  - Pulsar backend should still work but still needs refactoring
  - dataclassjson added as dependency
  - @dataclass decorator is used for data classes that can be easily converted to json
      - seems to be possible to use it bi-directionally
  - Also using Enums allows for more compact type checking and auto-complete in IDEs
  - Victor: Docs strings must comply with `numpy` style as specified for quantify packages.

Criticals:

- Installation woes within MacOS due to ruamel-yaml package. Core: #142
  - No good solution found yet
  - Can be a problem in the future
  - Could potentially be solved with anaconda installation

Discussion points:
- Core version bump to 0.3.1?
  - Should include a changelog (please keep it updated). Only relevant parts for the users should be included here. Details can be left out.
  - Schedule it for next week? Nothing seems to be blocking
  - Maintainers do not have enough bandwidth this week
  - If Victor finishes the iterative outer loops with inner batch
- Data structure for storing analysis quantities of interest. #140
  - Needs a summary of the current state
- Analysis class. #63

Review commits:
- Core key MRs. !88 !86 !93 (Thanks Victor for keeping a running changelog)

Any other business:
- Quantify-utilities repo is open for everyone to contribute to. It contains some example experiments and some general utility functions. Contains working experiments.
  - Details about what goes in there and where exactly to keep it are left for later
  - Intended as a central, shared utility repo for many end-users to be able to access


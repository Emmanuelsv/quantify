# Retrospective 
- discuss the process (general discussion)
    - what works well, what does not work well 
    - pain points submitted by team members 
- is mid-sprint 

## Date 17-12-2020

- Introduction of Thomas Reynders
- Process slides on Agile protocols by Callum will be added to the GitLab quantify organization to be accessible to all developers. 

## Quantify - core 
- good progress. 
- More mature and simpler than quantify-scheduler. 
- We are happy that we split the repositories. 

### Plotting monitor persistence mode 
- Took longer than expected because good interface was required. Now we have a good interface. 

Callum: Review meeting is for reviewing process 

Victor
- mostly limited by bandwidth/manpower which delayed the review cycle. 
- Feature needed some thought which also explains why it was bounced back a few times. 

### Installation process windows 
- Windows CI is only run on develop branch. 
- Solution (Conda) works for now, but we still need a proper solution. 
- Docker was briefly discussed, expertise to set this up is available in the group but we will not make this a priority right now. 

### quantify-scheduler 

Missed two internal deadlines, (release 0.2.0). Need to evaluate this. 
- More manpower available should alleviate problems. 
- Adriaan should focus more on preparing the work rather than implementing. 


## Next meeting
Next meeting is January 7th, that will be a review meeting. There we will review all issues/MR of that respective sprint. Ask everyone if they agree with solution or if needs to be reopened. 
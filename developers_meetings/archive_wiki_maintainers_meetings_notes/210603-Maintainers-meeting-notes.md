Note: Regular review meeting

Present:

## Current state (Max 30 mins):

### Quantify-core:
* quantify-core!180: Interpolated ND analysis pending. 
  - Not urgent

### Quantify-scheduler:
* We have released v0.3.0!

Key issues and MRs left:
* Review milestones and priorities for v0.4.0

## Extra-discussions:
* Decide on new procedure/labels for MR and Issues? Elaborate on #16
  - Decided to create a new issue and implement new labels
* quantify-core#205: Test datasets in RTD (decide)
  - Finish in separate MR for 0.4 milestone
* ControlStack discussion outcome? issues quantify-scheduler#131 quantify-scheduler#66
  - Design not concrete yet
  - Finish in separate MR for 0.4 milestone
  - Review !70 first, then !112
* Math on quantify-scheduler!124 checks out. (decide) 


## AOB (max 5 mins):
## Current state (Max 15 mins) [NOT DISCUSSED]: 
* Added scheduled pipeline (nightly)

### Quantify-core [NOT DISCUSSED]:
* CI and docs updates mirroring latest scheduler changes 
quantify-core!136 (extra discussion for end of meeting)
* set_datadir does not check for valid paths
quantify-core#167
* Plot monitor crashing for very large scans
quantify-core#168
* utilities.experiment_helpers.load_settings_onto_instrument does not work in practical setting quantify-core#166 (Adam has enough info I suppose?)
* Adaptive plotting crashing quantify-core#61 (7 months, no minimal example to reproduce)
* Add more tests for plotmon (quantify-core!85) quantify-core#130 (Still opened or fixed?)

### Quantify-scheduler [NOT DISCUSSED]:
* Acquisitions protocol quantify-scheduler!51 (Finally!). The new issues?
* Example spectroscopy programs quantify-scheduler!64

## Discussions:
### Urgent discussion (max 20 mins):
* Quantify-scheduler (relevant both repos) quantify-scheduler#85 Kelvin:
> whenever there are conflicts, please call the relevant person/committer? [Keep it open until I have some more information on how it happened with our process]
* Discussed:
  - This should have been spotted in the MR. 
  - Is probably a result of it being too big.

* Decision on docstrings format/sphinx extensions/`TYPE_CHECKING`/etc.
   - Main question: keep using `sphinx-autodoc-typehints` quantify-core!113 (and `scanpydoc.elegant_typehints` quantify-scheduler!67) or not?
   - Detailed comparison, Cons & Pros quantify#10 
* Discussed:
  - Using the module would require editing existing code.
  - It is also incompatible with '`TYPE_CHECKING`'
  - Resulting docstrings are considered nicer
  - Decision postponed until next meeting


### Quantify-core (max 15 mins):
* Discussion for Version 0.4.0. Prioritization still valid? What next? quantify-core#163
* Other technical issues raised?

### Quantify-scheduler (max 20 mins):
* v0.3.0 prioritization and bandwidths
[quantify-scheduler#66 quantify-scheduler#67 quantify-scheduler#68, quantify-scheduler#78 quantify-scheduler#79 quantify-scheduler#81]

Decision from project owners:

  - v0.3 -> 2 functional backends
  - v0.4 -> Either/and support for return types of measurements or control flow

* Other technical issues raised?

## Any Other Business (5 mins) [NOT DISCUSSED]:
* quantify-scheduler lacks code and essential documentation, e.g. difficulty in reviewing quantify-scheduler!64
* Quantify repos bandwidth issue persists (?)
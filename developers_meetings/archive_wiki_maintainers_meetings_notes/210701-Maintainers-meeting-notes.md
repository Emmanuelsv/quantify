Present:

## Current state (Max 30 mins):

### Quantify-core:

* Docker Unix containers are ready.
  - Docker testing environment ready for review
* No possible solution for Windows testing stage. Move to manual only to be triggered by maintainers prior to merge. See quantify-core!196

### Quantify-scheduler:

* ControlStack renamed to InstrumentCoordinator quantify-scheduler!151
* ZI backend tested to work. But need discussion on LocalOscillators InstrumentCoordinatorComponent design and implementation.
  - Kelvin to finsh Thomas's merge requests

* Working on tutorials for for experiments_transmon and quantify_scheduler

* Device objects and transmon element - in scheduler or experiments_transmon?


## Requested discussions:

* Stale MRs and issues: (Definition of Quantify dataset) quantify-core#187, quantify-core#139, quantify-core!111

## AOB (max 5 mins):
* UnitaryFund meeting this 7th July. Anyone else can make it?
  - Victor to attend
  - Release notes
  - Issues addressed/not addressed (user feedback)
  - Outreach (quantum Delft seminar)
  - Use case outside quantum computing
  - Kelvin ask Amber to produce two slides

- Propose to submit a minimal reproducible example for issues which break code? Notebook or script will do.

- 0.3.1 of quantify-core is out

- anything else missing from the analysis class thru the testing?


- quantify-scheduler #59, #60 lacks discusssion? is it blocking?
  * Suggested to replace specific `zhinst-backend` with more general `backend` tag.
  * Jules pointed out that he has some ideas about it which will be addressed in the qblox backend. Those same ideas could be adopted for zhinst.


- Discussion: on order of delivering features
  * zhinst-backend must adopt measurement protocols due to critical changes
  * Order of deliverables
     * Measurement protocols v0.3.0 4th March
     * Control stack object v0.3.0 4th March
     * Refactor configuration files
     * Add new standard library containing experiments (Bell, T1, T2... )
     * zhinst-backend
     * Adopt helper methods implemented in zhinst-backend for qblox-backend.

- from last meeting: damien to create MR for the qblox hardware?

- demo proof of concept from technical discussion ? Damien <2 days ago>
  * Discussion about the added value of the Concrete ControlStack Component classes.
     * Concerns whether it solves a problem or if it just adds another level of abstraction.
     * Adriaan suggests to create proof of concept so that it can be reviewed.
     * Create a new issue where this disussion is continued.

- Batched mode (Victor)
  * Victor pointed out that his work on batched mode is nearly finished and will be put up for review.
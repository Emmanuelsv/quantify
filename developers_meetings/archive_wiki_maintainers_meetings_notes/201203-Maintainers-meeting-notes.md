Review meeting 
- Adriaan and Callum have been working on resources refactor 

Individual MR's are being discussed
- We want to investigate issue #100 because it does clearly show how to set this but the examples don't follow best-practice, which is to use a single common data directory for all notebooks/experiments that are on a single measurement setup/PC. 
- Motivation is that if you don't do this, the tools we made to find/search data files or a potential data browser we might want to make don't work. 
- Adriaan will reopen this issue and adds the notes. 

- discussion on resource-refactor branch 
     - blocking and want it to merge but lots of work required 
     - is this extreme enough to break best practices? 
- concluded that we want to treat resources-refactor as an "issue-branch" we will have separate issues that merge into this branch. We focus on the refactor release and triage issues appropriately. 

Discussion on release 
- should be called 0.3.0 Adriaan will communicate
- Want to hold up the release for the persistence mode
- after that everyone is happy to release quantify-core. 
- Target release date for quantify-core 0.3.0 is next week Thursday. 

- Currently there is no milestone for minor (0.x.0) releases. We have identified that it is useful to have this. We will rename the current milestone for the first fully-featured release "Quantify v1.0.0" Callum will arrange this. 






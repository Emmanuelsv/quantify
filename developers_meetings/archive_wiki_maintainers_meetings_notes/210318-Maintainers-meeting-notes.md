## Current state (Max 15 mins): 
### Quantify-core:

* Release 0.3.2
* xarray bug to be fixed. There's a workaround by Victor. !131 
* Potential for xarray latest version to be breaking [backend api]? #161
* Updated install instructions. !132

Conclusion:
Keep a close eye on the changes. They may break everything

### Quantify-scheduler:

* Installation instructions !63
* Repo cleaned up. !59
* Added filename sanitization function for the pulsar backend (!60, !61)
* Pulsar time-out fixed? !58
  - Currently a workaround is in place
  - Root cause has not yet been identified

## Discussions:

### Urgent reflection (No max until resolved):

* Thomas and Victor to clear the communication procedure for MRs and commits. MR with changes were overwritten. (Details to be discussed)

* pylint fight conclusion:
  - Victor acidentally overrode Thomas' changes
  - Discuss closely when making changes to other people's MR
  - Stick to the process
  - Stick to the process, and don't bypass review

### Quantify-core (max 10 mins):

* Victor: Care to say a few words about the bool patch implementation? !131
  - version pinned to prevent breaking changes from develop branch of x array
  - hotfix np.bool -> bool
* Diplay format for lmfit parameters !133
  - bug in analysis identified
  - fixed by setting precision via standard error
  - need to deal better with dimensionless quantities (scaling via prefixes)
  - Adriaan has an opinion on how to deal with this (SI prefixes user setting)
* Other technical issues raised?

### Quantify-scheduler (max 15 mins):

* Acquisition protocols (too big!) !51
  - Ready to be merged. 
  - Spawn issues from open tasks
  - Poke reviewer

* Other technical issues raised?

## AOB (5 mins):
* No let's get back to march meeting/measuring
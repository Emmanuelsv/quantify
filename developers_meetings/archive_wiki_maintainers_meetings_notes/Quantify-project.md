This wiki serves to track decisions in the Quantify project. 

The board of the Quantify consortium consists of 
- Jules van Oven, CTO Qblox
- Adriaan Rol, Director of R&D, Orange Quantum Systems 

Current product owners of Quantify are:
- Jules van Oven
- Adriaan Rol

Current maintainers of Quantify are:
- Victor Negirneac
- Kelvin Loh
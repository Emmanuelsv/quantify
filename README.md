# Quantify

![Quantify logo](https://orangeqs.com/logos/QUANTIFY_LANDSCAPE.svg)

Quantify is a Python-based data acquisition framework focused on Quantum Computing and
solid-state physics experiments.
Currently it is a metapackage for [quantify-core](https://pypi.org/project/quantify-core/)
([git repo](https://gitlab.com/quantify-os/quantify-core/))
and [quantify-scheduler](https://pypi.org/project/quantify-scheduler/)
([git repo](https://gitlab.com/quantify-os/quantify-scheduler/)).
They are built on top of [QCoDeS](https://qcodes.github.io/Qcodes/) and are a spiritual
successor of [PycQED](https://github.com/DiCarloLab-Delft/PycQED_py3).

Take a look at the latest documentation for
[quantify-core](https://quantify-quantify-core.readthedocs-hosted.com/) and
[quantify-scheduler](https://quantify-quantify-scheduler.readthedocs-hosted.com/)
for the usage instructions.

## Overview and Community

For a general overview of Quantify and connecting to its open-source community,
see [quantify-os.org](https://quantify-os.org/).
Quantify is maintained by the Quantify Consortium consisting of Qblox and
Orange Quantum Systems.

[<img src="https://cdn.sanity.io/images/ostxzp7d/production/f9ab429fc72aea1b31c4b2c7fab5e378b67d75c3-132x31.svg" alt="Qblox logo" width=200px/>](https://qblox.com)
&nbsp;
&nbsp;
&nbsp;
&nbsp;
[<img src="https://orangeqs.com/OQS_logo_with_text.svg" alt="Orange Quantum Systems logo" width=200px/>](https://orangeqs.com)

&nbsp;

The software is licensed under a [BSD 3-clause license](https://gitlab.com/quantify-os/quantify/-/raw/main/LICENSE).
